import os
import datetime
import argparse
import time
import json
import queue

import multiprocessing as mp
from multiprocessing.connection import Connection

from typing import Literal
from datetime import datetime
from dataclasses import dataclass

import ublox
import trdp
import obd2


def _odometry_process_fnc(communicator: ublox.ParallelCommunicator, reciever: Connection, vehicle_type: Literal['none', 'car', 'tram', 'debug']) -> None:

  def create_speed_message(speed: int) -> ublox.UBXMessage: # TODO make sense of this lol :D
    timetag = int((time.time()*1000) % 4294967295).to_bytes(4, byteorder='little', signed=False)
    num_of_measurements = int(1 << 11).to_bytes(4, byteorder='little', signed=False) #also contains flags which we set to zero
    hex_speed = int(1000*speed/3.6).to_bytes(3, byteorder='little', signed=False)
    speed_type = b'\x0b'
    payload = b''.join([timetag, num_of_measurements, hex_speed, speed_type])
    return ublox.UBXMessage(16, 2, ublox.SET, payload=payload)

  def create_wheel_tick_message(count: int) -> ublox.UBXMessage:
      timetag = int((time.time()*1000) % 4294967295).to_bytes(4, byteorder='little', signed=False)
      num_of_measurements = int(1 << 11).to_bytes(4, byteorder='little', signed=False)
      negative = False
      if count < 0:
          negative = 1
      count = abs(count)
      if negative:
          count = count | 0x00800000
      tmp = int(count).to_bytes(3, byteorder='little', signed=False)
      tmp = b''.join([tmp, int(10).to_bytes(1, byteorder='little', signed=False)])
      pload= b''.join([timetag, num_of_measurements, tmp])
      return ublox.UBXMessage(16,2,ublox.SET,payload=pload)



  print("Started odometry process.")


  odometry: obd2.OBDReader|None # TODO add type for trdp reader
  class OdoDebug:
    def __init__(self) -> None:
      pass
    def get_speed(self) -> int:
      return 0
    
    def close(self) -> None:
      pass


  if vehicle_type == 'car':
    odometry = obd2.OBDReader()
    odometry.set_protocol(0)
    time.sleep(0.1)
    odometry.flush_serial()
    time.sleep(0.1)
    odometry.get_speed()
    while loop:
      speed: int = odometry.get_speed()
      # TODO add speed check condition
      time.sleep(0.1)
      communicator.write(create_speed_message(speed))
      if reciever.poll(0) and reciever.recv() == None:
        loop = False


  elif vehicle_type == 'tram':
    # todo add odometry
    odometry = trdp.TRDP()
    while loop:
      ticks = 0 #TODO get ticks

      time.sleep(0.1)
      communicator.write(create_wheel_tick_message(ticks))
      if reciever.poll(0) and reciever.recv() == None:
        loop = False


  elif vehicle_type == 'debug':
    odometry = OdoDebug()

  loop: bool = True


    
  odometry.close()
  print("Ending odometry process.")
  
def _ntrip_process_fnc(communicator: ublox.ParallelCommunicator, reciever: Connection) -> None:
  print("Starting NTRIP process.")

  ntrip_config_file = open("tram_config_files/tram_0-2_ntrip.json", 'r')
  ntrip_config: dict = json.loads(ntrip_config_file.read())
  ntrip_config_file.close()

  ntrip_config["REFLAT"] = ublox.coords_int2float(ntrip_config["REFLAT"][0], ntrip_config["REFLAT"][1], ntrip_config["REFLAT"][2])
  ntrip_config["REFLON"] = ublox.coords_int2float(ntrip_config["REFLON"][0], ntrip_config["REFLON"][1], ntrip_config["REFLON"][2])

  ntrip_queue = queue.Queue()

  gnc = ublox.GNSSNTRIPClient(None, verbosity=ublox.VERBOSITY_LOW)
  streaming = gnc.run(
    server        = ntrip_config["NTRIP_SERVER"],
    mountpoint    = ntrip_config["MOUNTPOINT"],
    user          = ntrip_config["NTRIP_USER"],
    password      = ntrip_config["NTRIP_PASSWORD"],
    reflat        = ntrip_config["REFLAT"],
    reflon        = ntrip_config["REFLON"],
    refalt        = ntrip_config["REFALT"],
    refsep        = ntrip_config["REFSEP"],
    ggainterval   = ntrip_config["GGAINT"],

    output        = ntrip_queue,
  )

  print("RTCM messages are streaming: " + str(streaming) + ".")

  loop: bool = True

  while loop:
    while (not ntrip_queue.empty()):
      data = ntrip_queue.get()
      communicator.write(data[0], 0)
      
    if reciever.poll(0) and reciever.recv() == None:
      loop = False
    
  print("Ending NTRIP process.")



if __name__ == "__main__":

  # argument handeling
  parser = argparse.ArgumentParser()

  DEFAULT_COMPORT: str
  if os.name == 'nt':
    DEFAULT_COMPORT = "COM8"
  else:
    DEFAULT_COMPORT = ublox.find_port('u-blox')


  parser.add_argument('--vehicle', type=str, default='none', help='Specify vehicle type: eg. car, tram or none.')
  parser.add_argument('--ubx_comport', type=str, default=DEFAULT_COMPORT, help='Specify comport (on Windows default is COM8), on Unix default is searched for by finder.')
  parser.add_argument('--timestamp', action=argparse.BooleanOptionalAction, default=True, help='Enable or disable timestamps.')
  parser.add_argument('--commlog', action=argparse.BooleanOptionalAction, default=True, help='Enable or disable communication logs.')
  
  args = parser.parse_args()

  @dataclass
  class Args:
    vehicle_data_type: Literal['none', 'car', 'tram', 'debug'] = args.__dict__['vehicle']
    timestamp_enable: bool = args.__dict__['timestamp']
    ubx_comport: str = args.__dict__['ubx_comport']
    log_enable: bool = args.__dict__['commlog']

  del parser, args

  communicator = ublox.ParallelCommunicator(Args.ubx_comport, ublox.FASTEST_BAUDRATE, ublox.BASE_TIMEOUT, log=Args.log_enable)
  communicator.start()

  # timesatamp sync
  TIME_SYNC_TRIES: int = 32
  if Args.timestamp_enable:
    print("Loading timestamp...")
    TIMESTAMP_FILENAME = "ublox_timestamp.log"
    optimal_pc_time: int|None = None
    optimal_ubx_time: int|None = None

    for i in range(TIME_SYNC_TRIES):
      print(str(i) + "/" + str(TIME_SYNC_TRIES), end='\r')
      pc_time: int
      message: ublox.UBXMessage|None = None
      while message == None:
        message = communicator.read()
        pc_time = time.time_ns() // (10**6)
        if type(message) != ublox.UBXMessage or not 'iTOW' in message.__dict__:
          message = None

      if (optimal_pc_time == None or optimal_ubx_time == None) or (pc_time - message.__dict__['iTOW'] > optimal_pc_time - optimal_ubx_time):
        optimal_pc_time = pc_time
        optimal_ubx_time = message.__dict__['iTOW']

    timestamp_file = open('TIMESTAMP_FILENAME', 'a')
    timestamp_file.write(str(datetime.now()) + ", " + str(optimal_pc_time) + ", " + str(optimal_ubx_time) + "\n")
    timestamp_file.close()

    print("Timestamp loaded.")

    del optimal_pc_time, optimal_ubx_time
    del pc_time, message
    del timestamp_file


  # config loading
  print("Loading config...")
  config: ublox.UBXConfig = ublox.UBXConfig.get_from_json('tram_config_files/tram_0-2_ublox.json')
  config.set_into_ublox(communicator)
  print("Config loaded.")


  if Args.vehicle_data_type != 'none':
    odometry_process_reciever, odometry_process_sender  = mp.Pipe(duplex=False)
  ntrip_process_reciever, ntrip_process_sender = mp.Pipe(duplex=False)
  
  if Args.vehicle_data_type != 'none':
    odometry_process = mp.Process(target=_odometry_process_fnc, args=(communicator, odometry_process_reciever, Args.vehicle_data_type))
  ntrip_process = mp.Process(target=_ntrip_process_fnc, args=(communicator, ntrip_process_reciever))

  if Args.vehicle_data_type != 'none':
    odometry_process.start()
  ntrip_process.start()

  end_loop: bool = False
  while not end_loop:
    inp: str = input()

    if inp == "q":
      end_loop = True
    elif inp == "Somebody":
      print(""" Somebody once told me the world is gonna roll me\n
                I ain't the sharpest tool in the shed\n
                She was looking kind of dumb with her finger and her thumb\n
                In the shape of an "L" on her forehead""")

  if Args.vehicle_data_type != 'none':
    print("Sending end command to odometry process.")
    odometry_process_sender.send(None)
    odometry_process.join()
    print("Odometry process ended.")

  print("Sending end command to NTRIP process.")
  ntrip_process_sender.send(None)
  ntrip_process.join()
  print("NTRIP process ended.")

  print("Stopping communicator.")
  communicator.stop()
  print("Communicator stopped.")
  
  print("Script ended.")
    