import ublox.unix_comport as comport
import ublox.gnss_time as gnsstime
import ublox.communicators as comm
import pyubx2
import pygnssutils
import serial
import multiprocessing as mp
import threading as th
import keyboard
import time
import sys
import getopt

from queue import Queue as qu

sys.path.append(sys.path[0] + "/commstest/gnss")
sys.path.append(sys.path[0] + "/obd2/obd_comm")

from gnss_com_keys import cfg_keys as cfg
from gnss_com import *
from obd_comm import *

from ublox.communicators.communicator import * 

def getargs(argv):
    arg_input = ""
    arg_do_timestamp = True
    arg_vehicle = "none"
    arg_help = "{0} -t <do_timestamp> -v <vehicle>".format(argv[0])
    
    try:
        opts, args = getopt.getopt(argv[1:], "h:t:v:", ["help", 
        "timestamp", "vehicle"])
    except:
        print(arg_help)
        sys.exit(2)
    
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(arg_help)  # print the help message
            sys.exit(2)
        elif opt in ("-v"):
            arg_vehicle = arg
        elif opt in ("-t", "--time"):
            if arg == "0":
                arg_do_timestamp = False
    return arg_do_timestamp, arg_vehicle




if __name__ == "__main__":
  TIMESTAMP_FILENAME = "timestamp.txt"
  UBLOX_COMPORT: str = comport.find_port("u-blox")
  UBLOX_BAUDRATE: int = comm.FASTEST_BAUDRATE
  UBLOX_TIMEOUT: float = 2.0
  
  do_timestamp, vehicle =getargs(sys.argv)

  if do_timestamp:
    # TIMER SYNC
    communicator = comm.UBXCommunicator(UBLOX_COMPORT, UBLOX_BAUDRATE, UBLOX_TIMEOUT)
    timer = gnsstime.GNSSFixedTimer(communicator=communicator)
    with open("logs/"+TIMESTAMP_FILENAME, 'a') as timestamp_file:
      timestamp_file.write("gnss_timestamp: " + str(timer.get_gnss_timestamp()) + "\nposix_offset: " + str(timer.get_posix_offset()))
    

    communicator.close()
    del communicator
    # END OF TIME SYNC
    print("Debug: Time sync ended.")
  else:
    print("skipping timestamp init")
  


  parallel_communicator: ParallelCommunicator = ParallelCommunicator(UBLOX_COMPORT, UBLOX_BAUDRATE, UBLOX_TIMEOUT)

  process_ender = ProcessEnder()

  def reader_log_process_fnc(parallel_communicator: ParallelCommunicator, process_ender: ProcessEnder) -> None:
    RAW_LOG_NAME: str = "raw_read.log"
    PARSED_LOG_NAME: str = "parsed_read.log"

    raw_log = open("logs/"+RAW_LOG_NAME, 'wb')
    parsed_log = open("logs/"+PARSED_LOG_NAME, 'a')

    tmptime = 0
    
    calibrated = False
    parsed_log.write("START \n")
    while not calibrated and not process_ender.is_end():
      data = parallel_communicator.read()

      if type(data) == tuple:
        if data[1]:
          if "iTOW" in data[1].__dict__:
            tmptime = data[1].__dict__["iTOW"]
          if "status" in data[1].__dict__:
            print(data[1])
            if data[1].__dict__["status"] > 2:
              calibrated = True
        if type(data[0]) == bytes:
          raw_log.write(data[0])
        parsed_log.write(data[1].__str__() +" " + str(tmptime)+", "+ str(time.time())+"\n")

    parsed_log.write("-----------------------\n")


    parallel_communicator.write(get_config_msg([  
                  (cfg.CFG_RATE_MEAS, b'\x64\x00'), #min is 0x19 but when 0x19 is chosen together with rate nav 2 and CFG-MSGOUT-UBX_NAV_POSLLH_USB = 1 it starts crashing
                  (cfg.CFG_RATE_NAV, b'\x01\x00'),
                  (cfg.CFG_RATE_NAV_PRIO, b'\x0a'), #turns on priority nav mode
                  (cfg.CFG_MSGOUT_UBX_ESF_MEAS_USB, b'\x10'),
                  (cfg.CFG_SBAS_USE_RANGING, b'\x01')   #use SBAS corrections while ranging
                ]).serialize(), 0)


    print("all sensors are calibrated, turning on PRIORITY NAV MODE")    
    while not process_ender.is_end():
      data = parallel_communicator.read() #tady je problem, se tam sekne
      
      if type(data) == tuple:
        if "iTOW" in data[1].__dict__:
          tmptime = data[1].__dict__["iTOW"]
        #if "fusionMode" in data[1].__dict__:
          #print(data[1])
        if type(data[0]) == bytes:
          raw_log.write(data[0])
        parsed_log.write(data[1].__str__() +" " + str(tmptime)+", "+ str(time.time())+"\n")
    
    raw_log.close()
    parsed_log.close()
    print("Debug: Reader log ended.")
  
  def odometry_process_fnc(parallel_communicator: ParallelCommunicator, process_ender: ProcessEnder, odo_type: int) -> None:
    OBDLOG_FILE = "logs/obd_log.txt"

    if odo_type == "car":
      print("Debug, obd setup begins")
      
      obd: OBDReader = OBDReader(comport="/dev/ttyUSB0", baudrate=38400, timeout=1.0/10.0, end='\r')

      if obd.restart_setup():
        print("OBD setup done.")
      else:
        print("OBD setup failed.")
      sleep(0.1)
      obd.start_updates()
      obdlog = open(OBDLOG_FILE, 'a')
      
      speed = 0
      while speed < 5:
        speed = obd.get_speed()
        time.sleep(0.2)
      
      print("Debug: odometry works")
      while True:
        speed = obd.get_speed()
        throttle = obd.get_throttle()
        parallel_communicator.write(get_speed_msg(speed).serialize(), 0)
        obdlog.write(str(speed) + ", " + str(throttle) + ", " + str(time.time()) + "\n")
        time.sleep(0.05)


    elif odo_type == "tram":
      while not process_ender.is_end():

        relative_count = 0 # TODO

        data = get_wheel_tick_msg(relative_count).serialize()
        parallel_communicator.write(data, 0)
        time.sleep(0.01)
    print("ending odo process")




  def ntrip_recieve_process_fnc(parallel_communicator: ParallelCommunicator, process_ender: ProcessEnder) -> None:
    print("starting NTRIP client")
    NTRIP_SERVER = "czeposr.cuzk.cz"
    NTRIP_PORT = 2101
    MOUNTPOINT = "CPRG3-MSM"
    NTRIP_USER = "fel_control"
    NTRIP_PASSWORD = "mamradGNSS"
    REFLAT =50+7/60+30.82619/3600 	  	 	  	
    REFLON = 14+27/60+21.80473/3600
    REFALT = 356.025
    REFSEP = 0
    GGAINT = -1

    ntrip_queue = qu()

    with pygnssutils.GNSSNTRIPClient(None, verbosity=pygnssutils.VERBOSITY_LOW) as gnc:
      streaming = gnc.run(
          server=NTRIP_SERVER,
          mountpoint=MOUNTPOINT,
          user=NTRIP_USER,
          password=NTRIP_PASSWORD,
          reflat=REFLAT,
          reflon=REFLON,
          refalt=REFALT,
          refsep=REFSEP,
          ggainterval=GGAINT,
          output=ntrip_queue,
      )
      print("RTCM messages are streaming: " + str(streaming))
      while not process_ender.is_end():
        while (not ntrip_queue.empty()):
          data = ntrip_queue.get()
          parallel_communicator.write(data[0],0)

  
  reader_log_process: mp.Process = mp.Process(target=reader_log_process_fnc, args=[parallel_communicator, process_ender])
  odometry_process: mp.Process = mp.Process(target=odometry_process_fnc, args=[parallel_communicator, process_ender, vehicle])
  ntrip_recieve_process: mp.Process = mp.Process(target=ntrip_recieve_process_fnc, args=[parallel_communicator, process_ender])

  reader_log_process.start()
  odometry_process.start()
  ntrip_recieve_process.start()

  print("Debug: Parallel processes started.")

  with open("logs/+mytimestamps.txt", 'a') as timestamp_file:
    while not keyboard.is_pressed('q'):
      if keyboard.is_pressed('p'):
        tmptime = str(time.time())
        print(tmptime)
        timestamp_file.write(tmptime + "\n")
        time.sleep(0.2)
  process_ender.set_end()

  print("Debug: Ending beacon sent.")

  reader_log_process.join()
  print("log process ended")
  odometry_process.join()
  print("odoprocess ended")
  ntrip_recieve_process.join()

  print("done")
