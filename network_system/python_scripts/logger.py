# * Name:   logger.py
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_14

# * HELP:
#   * Controls:
#     * q - quit
#     * h - help
#     * c - check for thread health

from io import TextIOWrapper
import sys
import toml
import threading
import time

from messages_pb2 import *

import ecal.core.core as ecal_core
from ecal.core.subscriber import ProtoSubscriber

def print_debug(msg: str) -> None:
  print("logger.py: " + msg)


class ThreadEnder:
  def __init__(self) -> None:
    self.__end: bool = False
    self.__lock = threading.Lock()
  
  def set_end(self, end: bool = True) -> None:
    self.__lock.acquire()
    self.__end = end
    self.__lock.release()

  def get_end(self) -> bool:
    self.__lock.acquire()
    end: bool = self.__end
    self.__lock.release()
    return end

def loop_read(subscriber_list: list[tuple[ProtoSubscriber, TextIOWrapper, list[str]]], ender: ThreadEnder) -> None:
  for subscriber in subscriber_list:
    subscriber[1].write( 'timestamp_ns' )
    subscriber[1].write( ", " )
    subscriber[1].write( 'ecal_time' )

    for attr in subscriber[2]:
      subscriber[1].write( ", " )
      subscriber[1].write( attr )
    subscriber[1].write('\n')


  while not ender.get_end():
    if not ecal_core.ok():
      print_debug("eCal core error.")
      break



    for subscriber in subscriber_list:
      ret, obj, tm = subscriber[0].receive()
      if ret > 0:
        subscriber[1].write( str(time.time_ns()) )
        subscriber[1].write( ", " )
        subscriber[1].write( str(tm) )

        for attr in subscriber[2]:
          subscriber[1].write( ", " )
          subscriber[1].write( str(getattr(obj, attr)) )
        subscriber[1].write('\n')
      
        





if __name__ == "__main__":

  config = toml.load("configs/logger.toml")


  ecal_core.initialize(sys.argv, "py_logger")
  
  # set process state
  ecal_core.set_process_state(1, 1, "Logging script")


  channel_attributes: dict[str,list[str]] = dict()
  subscriber_list: list[tuple[ProtoSubscriber, TextIOWrapper, list[str]]] = list()

  for channel in config:
    if len(config[channel]) == 0:
      continue

    sub = ProtoSubscriber(channel, globals()["Message" + channel[0].capitalize() + channel[1:]])

    file = open(channel + ".csv", 'a')

    attr = config[channel]
    
    subscriber_list.append( (sub, file, attr) )

  ender = ThreadEnder()
  thr = threading.Thread(target=loop_read, args=[subscriber_list, ender])
  thr.start()

  while True:
    print_debug("Write your input, send with ENTER:")
    inp: str = input()
    if len(inp) == 0:
      continue
    elif inp == 'q':
      print_debug("Logger thread requested to quit.")
      ender.set_end()
      break
    elif inp == 'h':
      print_debug("For help look look into " + __file__ + ", there is help in the begining of the file.")
    
    elif inp == 'c':
      ret: str = "ERROR"
      if thr.is_alive():
        ret = "OK"

      print_debug("Thread health is: " + ret + ".")
    
    elif inp == 's':
      print_debug(" " + """
                      omebody once told me the world is gonna roll me\n
                      I ain't the sharpest tool in the shed\n
                      She was looking kind of dumb with her finger and her thumb\n
                      In the shape of an "L" on her forehead
                      """)
    else:
      print_debug("Wrong command, for help, enter 'h'.")

  
  thr.join()
  ecal_core.finalize()
  print_debug("Signing off.")
