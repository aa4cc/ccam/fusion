import sys
from dataclasses import dataclass
from typing import Iterable
import toml
import time
import threading
import queue

from pygnssutils.gnssntripclient import GNSSNTRIPClient, VERBOSITY_LOW

from messages_pb2 import MessageNtrip


import ecal.core.core as ecal_core
from ecal.core.publisher import ProtoPublisher

LOG_FILENAME: str = "log_ntrip.csv.csv"

class ThreadEnder:
  def __init__(self) -> None:
    self.__end: bool = False
    self.__lock = threading.Lock()
  
  def set_end(self, end: bool = True) -> None:
    self.__lock.acquire()
    self.__end = end
    self.__lock.release()

  def get_end(self) -> bool:
    self.__lock.acquire()
    end: bool = self.__end
    self.__lock.release()
    return end

def print_debug(msg: str) -> None:
  print("ntrip.py: " + msg)

def coord_imp2float(degrees: int|float, minutes: int|float = 0.0, seconds: int|float = 0.0) -> float:
  return float(degrees) + 60.0*( float(minutes) + 60.0*float(seconds) )


def loop_read(ntrip_config: dict, pub: ProtoPublisher, ender: ThreadEnder) -> None:
  file = open(LOG_FILENAME, 'a')
  file.write("timestamp_ns, data\n")
  ntrip_queue = queue.Queue()
  gnc = GNSSNTRIPClient(None, verbosity=1)#VERBOSITY_LOW)
  streaming = gnc.run(
    server        = ntrip_config["NTRIP_SERVER"],
    mountpoint    = ntrip_config["MOUNTPOINT"],
    user          = ntrip_config["NTRIP_USER"],
    password      = ntrip_config["NTRIP_PASSWORD"],
    reflat        = ntrip_config["REFLAT"],
    reflon        = ntrip_config["REFLON"],
    refalt        = ntrip_config["REFALT"],
    refsep        = ntrip_config["REFSEP"],
    ggainterval   = ntrip_config["GGAINT"],

    output        = ntrip_queue,
  )
  if streaming:
    print_debug("RTCM messages are streaming.")
  else:
    print_debug("There was error while setting up ntrip reciever.")
    exit(-1)

  
  while not ender.get_end():

    if not ecal_core.ok():
      print_debug("eCal core error.")
      break
      
    while (not ntrip_queue.empty()):
      data: bytes
      data = ntrip_queue.get()
      msg = MessageNtrip()
      msg.data = data[0]
      pub.send(msg)
      file.write(str(time.time_ns()) + ", " + data[0].hex() + "\n")
  
  file.close()



if __name__ == "__main__":
  config = toml.load("configs/ntrip.toml")

  ecal_core.initialize(sys.argv, "py_ntrip")

  ecal_core.set_process_state(1, 1, "NTRIP process")

  pub = ProtoPublisher(config['channel_name'], MessageNtrip)

  ender = ThreadEnder()

  thr = threading.Thread(target=loop_read, args=[config, pub, ender])
  thr.start()

  while True:
    print_debug("Write your input, send with ENTER:")
    inp: str = input()
    if len(inp) == 0:
      continue
    elif inp == 'q':
      print_debug("OBD thread requested to quit.")
      ender.set_end()
      break
    elif inp == 'h':
      print_debug("For help look look into " + __file__ + ", there is help in the begining of the file.")
    
    elif inp == 'c':
      ret: str = "ERROR"
      if thr.is_alive():
        ret = "OK"

      print_debug("Thread health is: " + ret + ".")
    
    else:
      print_debug("Wrong command, for help, enter 'h'.")
    
  thr.join()
  ecal_core.finalize()
  print_debug("Signing off.")
