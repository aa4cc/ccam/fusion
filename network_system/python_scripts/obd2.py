# * Name:   obd2_script.py
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_14

# * HELP:
#   * Controls:
#     * q - quit
#     * h - help
#     * c - check for thread health




import sys
from dataclasses import dataclass
from typing import Iterable
import toml
import time
import serial
import threading

from messages_pb2 import MessageOdometry


import ecal.core.core as ecal_core
from ecal.core.publisher import ProtoPublisher


LOG_FILENAME: str = "log_obd2.csv"

class ThreadEnder:
  def __init__(self) -> None:
    self.__end: bool = False
    self.__lock = threading.Lock()
  
  def set_end(self, end: bool = True) -> None:
    self.__lock.acquire()
    self.__end = end
    self.__lock.release()

  def get_end(self) -> bool:
    self.__lock.acquire()
    end: bool = self.__end
    self.__lock.release()
    return end


@dataclass
class OBDMessages:
  @dataclass
  class Send:
    RESTART: str = "ATZ"
    SPEED: str = "010D"

    PIDS: str = "0100"


    def set_protocol(protocol: int) -> str:
      return "AT TP" + str(protocol)
  
  @dataclass
  class Recieve:
    PROTOCOL_CONFIRM: str = "OK"

    SPEED: str = "410D"




def print_debug(msg: str) -> None:
  print("obd2.py: " + msg)


def send_message(port: serial.Serial, msg: str) -> bool:
  port.write((msg + '\r').encode('utf-8'))
  confirm = recieve_message(port)
  return confirm == msg or confirm == ">" + msg


def recieve_message(port: serial.Serial, end: str = '\r') -> str:
  out = str()
  while True:
    a = port.read(1)
    if a == b"\xfc":
      continue
    c: str = a.decode('utf-8')
    if c == '\r':
      break
    elif len(c) == 1:
      out += c
    elif len(out) > 0:
      continue
    else:
      break
  return out

def restart_chip(port: serial.Serial) -> bool:
  print_debug("Restarting chip...")
  send_message(port, OBDMessages.Send.RESTART)
  r1 =  recieve_message(port) == ""
  r2 =  recieve_message(port) == ""
  r3 =  recieve_message(port) == "ELM327 v1.5"
  r4 =  recieve_message(port) == ""
  ret = r1 and r2 and r3 and r4
  if ret:
    print_debug("Chip restarted.")
  else:
    print_debug("Error while restarting chip.")
  return ret


def set_protocol(port: serial.Serial, protocol: int) -> bool:
  if type(protocol) != int or protocol < 0:
    print_debug("Invalid protocol number.")
    return False

  print_debug("Setting protocol to " + str(protocol) + ".")
  send_message(port, OBDMessages.Send.set_protocol(protocol) )

  time.sleep(0.5)

  ret: bool = recieve_message(port) == OBDMessages.Recieve.PROTOCOL_CONFIRM and \
              recieve_message(port) == ""

  if ret:
    print_debug("Protocol set.")
  else:
    print_debug("Could not set protocol.")
  
  return ret


def find_protocol(port: serial.Serial, candidates: Iterable = range(10)) -> bool:

  print_debug("Trying to find protocol.")

  if not restart_chip(port):
    return False

  # try ATTP0
  selected_protocol = -1
  if 0 in candidates and False:
    if not set_protocol(port, 0):
      print_debug("Error while trying protocol 0.")
      return False

    send_message(port, OBDMessages.Send.PIDS)
    r1 = recieve_message(port) == "SEARCHING..."
    r2 = recieve_message(port) != "UNABLE TO CONNECT"
    r3 = recieve_message(port) == ""

    if r1 and r2 and r3:
      selected_protocol = 0
    elif not restart_chip(port):
      return False
  


  if selected_protocol < 0:
    print("test")
    for protocol in range(1, 10):
      if protocol not in candidates:
        continue

      if not set_protocol(port, protocol):
        print_debug("Error while trying protocol " + str(protocol + "."))
        return False
      
      send_message(port, OBDMessages.Send.PIDS)
      if recieve_message(port) == "BUS INIT: OK":
        recieve_message(port)
        recieve_message(port)
        selected_protocol = protocol
        break
      else:
        recieve_message(port)

  
  if selected_protocol < 0:
    print_debug("No suitable protocol was found.")
    return False
  else:
    print_debug("Protocol " + str(protocol) + " was selected.")
    return True

    



def loop_read(port: serial.Serial, ender: ThreadEnder, pub: ProtoPublisher, protocol: int) -> None:

  file = open(LOG_FILENAME, 'a')
  file.write("timestamp_ns, speed_km\n")

  while not ender.get_end():

    if not ecal_core.ok():
      print_debug("eCal core error.")
      break


    send_message(port, OBDMessages.Send.SPEED)


    for _ in range(5):
      rec: str = recieve_message(port).replace(" ", "")

      if len(rec) == 6 or OBDMessages.Recieve.SPEED in rec[:len(OBDMessages.Recieve.SPEED)]:

        speed = float( int(rec[len(OBDMessages.Recieve.SPEED):], base=16) )
        print
        payload = MessageOdometry()
        payload.speed = speed
        
        pub.send(payload)
        file.write(str(time.time_ns()) + ", " + str(speed) + "\n")
        if protocol == 7:
          recieve_message(port)

        print(speed, end="\r")
        break
      time.sleep(0.1)
  file.close()


def flush(port: serial.Serial) -> None:
  port.flushInput()
  port.flushOutput()
      
  


if __name__ == "__main__":

  config = toml.load("configs/obd2.toml")

  COMPORT: str = config['comport']
  BAUDRATE: int = config['baudrate']
  TIMEOUT: float = config['timeout']

  PROTOCOL: int = config['protocol']

  CHANNEL: str = config['channel_name']

  del config

  try:
    port = serial.Serial(port=COMPORT, baudrate=BAUDRATE, timeout=TIMEOUT)
  except:
    print_debug("Comport could not be reached, check whether the device is connected.")
    exit(-1)



  if not port.is_open:
    print_debug("Port was not opened.")
    exit(-1)

  restart_chip(port)


  if PROTOCOL < 0 and not find_protocol(port):
    exit(-1)
     
  elif PROTOCOL > 0:
    if not set_protocol(port, PROTOCOL):
      exit(-1)
    send_message(port, OBDMessages.Send.PIDS)
    time.sleep(2.0)
    flush(port)

  ecal_core.initialize(sys.argv, "py_obd2")
  
  # set process state
  ecal_core.set_process_state(1, 1, "OBD2 odometry process")

  pub = ProtoPublisher(CHANNEL, MessageOdometry)

  ender = ThreadEnder()

  thr = threading.Thread(target=loop_read, args=[port, ender, pub, PROTOCOL])
  thr.start()

  while True:
    print_debug("Write your input, send with ENTER:")
    inp: str = input()
    if len(inp) == 0:
      continue
    elif inp == 'q':
      print_debug("OBD thread requested to quit.")
      ender.set_end()
      break
    elif inp == 'h':
      print_debug("For help look look into " + __file__ + ", there is help in the begining of the file.")
    
    elif inp == 'c':
      ret: str = "ERROR"
      if thr.is_alive():
        ret = "OK"

      print_debug("Thread health is: " + ret + ".")
    
    else:
      print_debug("Wrong command, for help, enter 'h'.")
    
  thr.join()
  port.close()
  ecal_core.finalize()
  print_debug("Signing off.")
