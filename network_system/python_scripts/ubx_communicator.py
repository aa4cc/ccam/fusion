# * Name:   ubx_communicator.py
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

# * HELP:
#   * Controls:
#     * q - quit
#     * h - help
#     * c - check for thread health

import sys
import toml
import time
import serial
import threading
import pyubx2

from messages_pb2 import MessageOdometry, MessageNtrip, MessageGnss


import ecal.core.core as ecal_core
from ecal.core.publisher import ProtoPublisher
from ecal.core.subscriber import ProtoSubscriber

LOG_FILENAME: str = "log_ubx_communicator.csv"

class ThreadEnder:
  def __init__(self) -> None:
    self.__end: bool = False
    self.__lock = threading.Lock()
  
  def set_end(self, end: bool = True) -> None:
    self.__lock.acquire()
    self.__end = end
    self.__lock.release()

  def get_end(self) -> bool:
    self.__lock.acquire()
    end: bool = self.__end
    self.__lock.release()
    return end
    
def print_debug(msg: str) -> None:
  print("ubx_communicator.py: " + msg)

def create_speed_message(speed: int) -> pyubx2.UBXMessage: # TODO make sense of this lol :D
  timetag = int((time.time()*1000) % 4294967295).to_bytes(4, byteorder='little', signed=False)
  num_of_measurements = int(1 << 11).to_bytes(4, byteorder='little', signed=False) #also contains flags which we set to zero
  hex_speed = int(1000*speed/3.6).to_bytes(3, byteorder='little', signed=False)
  speed_type = b'\x0b'
  payload = b''.join([timetag, num_of_measurements, hex_speed, speed_type])
  return pyubx2.UBXMessage(16, 2, pyubx2.SET, payload=payload)

def create_wheel_tick_message(count: int) -> pyubx2.UBXMessage:
  timetag = int((time.time()*1000) % 4294967295).to_bytes(4, byteorder='little', signed=False)
  num_of_measurements = int(1 << 11).to_bytes(4, byteorder='little', signed=False)
  negative = False
  if count < 0:
    negative = 1
  count = abs(count)
  if negative:
    count = count | 0x00800000
  tmp = int(count).to_bytes(3, byteorder='little', signed=False)
  tmp = b''.join([tmp, int(10).to_bytes(1, byteorder='little', signed=False)])
  pload= b''.join([timetag, num_of_measurements, tmp])
  return pyubx2.UBXMessage(16,2,pyubx2.SET,payload=pload)


def loop_reader(ports: list[serial.Serial], ender: ThreadEnder, gnss_pub: ProtoPublisher, ntrip_sub: ProtoSubscriber, odometry_sub: ProtoSubscriber) -> None:

  file = open(LOG_FILENAME, 'a')

  file.write("timestamp_ns, port name, data\n")
  

  reader_list: list[pyubx2.UBXReader] = list()
  for port in ports:
    reader_list.append(pyubx2.UBXReader(port, protfilter=2))
  
  rng: range = range(len(ports))

  while not ender.get_end():

    if not ecal_core.ok():
      print_debug("eCal core error.")
      break

    # read ntrip
    ret, obj, _ = ntrip_sub.receive()
    if ret > 0:
      for i in rng:
        ports[i].write(obj.data)
    
    # read odometry
    ret, obj, _ = odometry_sub.receive()
    if ret > 0:
      if obj.HasField('speed'):
        for i in rng:
          ports[i].write(create_speed_message(int(obj.speed)).serialize())
      elif obj.HasField('ticks'):
        for i in rng:
          ports[i].write(create_wheel_tick_message(int(obj.ticks)).serialize())
    
    for i in rng:
      if ports[i].in_waiting > 0:
        raw: bytes
        recieved: pyubx2.UBXMessage
        raw, recieved= reader_list[i].read()
        if hasattr(recieved, 'status') or hasattr(recieved, 'difSoln'):
          print(recieved)
        file.write(str(time.time_ns()) + ", " + port.name + ", " + raw.hex() + "\n")



        
        if hasattr(recieved, 'lat') and hasattr(recieved, 'lon') and hasattr(recieved, 'height'):
          msg = MessageGnss()
          msg.reciever_id = i
          msg.lat = getattr(recieved, 'lat')
          msg.lng = getattr(recieved, 'lon')
          msg.alt = getattr(recieved, 'height')

          gnss_pub.send(msg)
  file.close()




if __name__ == "__main__":
  config = toml.load("configs/ubx_communicator.toml")

  COMPORTS: list[str] = config['comports']
  BAUDRATES: list[int] = config['baudrates']
  TIMEOUTS: list[float] = config['timeouts']

  GNSS_CHANNEL_NAME: str = config['gnss_channel_name']
  NTRIP_CHANNEL_NAME: str = config['ntrip_channel_name']
  ODOMETRY_CHANNEL_NAME:str =config['odometry_channel_name']


  #input checks
  if len(COMPORTS) != len(BAUDRATES) or len(BAUDRATES) != len(TIMEOUTS):
    print_debug("There is not the same number of comports, baudrates and timeouts.")
    exit(-1)
  
  if  any(type(x) != str for x in COMPORTS) or \
      any(type(x) != int for x in BAUDRATES) or \
      any(type(x) != float for x in TIMEOUTS) or \
      type(GNSS_CHANNEL_NAME) != str or \
      type(NTRIP_CHANNEL_NAME) != str or \
      type(ODOMETRY_CHANNEL_NAME) != str:
    print_debug("There is an error in respective config file.")
    exit(-1)


  # open ports
  ports: list[serial.Serial] = list()

  print_debug("Opening ports...")

  for i in range(len(COMPORTS)):
    try:
      new_port = serial.Serial(port=COMPORTS[i], baudrate=BAUDRATES[i], timeout=TIMEOUTS[i])
    except:
      print_debug("Could not open one of the comports, make sure all of them are specified correctly in respective config file.")
      for port in ports:
        port.close()
      exit(-1)
    ports.append(new_port)
  
  for port in ports:
    if not port.isOpen():
      print_debug("One of the ports was not opened.")
      for closeport in ports:
        closeport.close()
      exit(-1)
  
  print_debug("All ports were opened.")






  ecal_core.initialize(sys.argv, "py_ubx_communicator")
  
  # set process state
  ecal_core.set_process_state(1, 1, "UBX communicator process")

  gnss_pub = ProtoPublisher(GNSS_CHANNEL_NAME, MessageGnss)
  ntrip_sub = ProtoSubscriber(NTRIP_CHANNEL_NAME, MessageNtrip)
  odometry_sub = ProtoSubscriber(ODOMETRY_CHANNEL_NAME, MessageOdometry)

  ender = ThreadEnder()

  thr = threading.Thread(target=loop_reader, args=[ports, ender, gnss_pub, ntrip_sub, odometry_sub])
  thr.start()


  while True:
    print_debug("Write your input, send with ENTER:")
    inp: str = input()
    if len(inp) == 0:
      continue
    elif inp == 'q':
      print_debug("Communication thread requested to quit.")
      ender.set_end()
      break
    elif inp == 'h':
      print_debug("For help look look into " + __file__ + ", there is help in the begining of the file.")
    
    elif inp == 'c':
      ret: str = "ERROR"
      if thr.is_alive():
        ret = "OK"

      print_debug("Thread health is: " + ret + ".")
    
    elif inp == 'r':
      try:
        import webbrowser
        webbrowser.open("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
      except:
        print_debug(":(")
    
    else:
      print_debug("Wrong command, for help, enter 'h'.")

  
  thr.join()

  print_debug("Closing ports...")
  for port in ports:
    port.close()


  ecal_core.finalize()
  print_debug("Signing off.")
