:: * Name:   compile.bat
:: * Author: Matej Kriz, krizmat3@fel.cvut.cz
:: * Date:   2022_10_19

@ECHO OFF
call %~dp0protobuf_messages\parse_messages.bat

:: moving files to their respective folders
move %~dp0protobuf_messages\messages_pb2.py %~dp0\python_scripts\messages_pb2.py
