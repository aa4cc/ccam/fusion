# Network system build on eCAL data sharing system
  <br>[eCAL Github](https://github.com/eclipse-ecal/ecal)
  <br>[Protobuf by Google](https://developers.google.com/protocol-buffers)
  <br>[ELM327 OBD2 Convertor Wiki](https://en.wikipedia.org/wiki/ELM327)
  <br>[pyubx2 Python Library for UBX Parsing](https://github.com/semuconsulting/pyubx2)
  <br>
  <br>[Really simple tutorial](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

# TL;DR
Launch script to install dependencies, more details at [Utility Scripts](/network_system/Readme.md#utility-scripts),
look for install_dependencies script. <br>
Launch either [tram_0_3_win.bat](../tram_0_3_win.bat) or [tram_0_3_lnx.sh](../tram_0_3_lnx.sh) to start the whole thing.


## Operating system

## Ubuntu
This library is intended primarilly for Ubuntu and simmilar operating systems using GNOME terminal, there should be no issuses (I'am running pop_OS so it should also work).

## Other Linux
Should be easy to convert, .sh files apart from utility scripts for Ubuntu should be working just fine,
just in case you are not using GNOME you should use other terminal command.

## Windows
Well now the system works on windows properly, but sometimes pip installs wrong version (it says that it cant import ecal builder)
of protobuf, so launch cmd or powershell as administrator and type ``pip install protobuf --upgrade`` that should resolve the issue.  

For example /python_scripts/ files should be run from pwd .../network_system/ so they are capable of loading their config file.

## MacOS
lmao


## Utility scripts
Batch of scripts doing some more tiring tasks

### Ubuntu
  There is a directory with utility and quality of life scripts under ./utility_scripts/ubuntu/, they are meant for Ubuntu but should work for most Debian based distros.
  Do not call them via ``$ sh`` command, rather use ``chmod u+x ./file.sh`` and run them from immideate directory as ``./file.sh``.

  [install_dependencies.sh](utility_scripts/ubuntu/install_dependencies.sh) will install all needed dependencies of the main script, including pip and eCAL,
  notably it will kick you out if you do not have Python vesion 3.10 or higher (included in Ubuntu 22.04 and younger, usually it is easier to upgrade
  your system than trying to change default Python interpreter).

  [install_pygpsclient.sh](utility_scripts/ubuntu/install_pygpsclient.sh) will install pygpsclient and its dependencies, a notoriously reterded task to do manually.

  [fix_obd_port_not_displaying.sh](utility_scripts/ubuntu/fix_obd_port_not_displaying.sh) script for Debian based distros who think OBD2 is braille keyboard for
  some accursed reason.

  [fix_nonsudo_access_to_ports.sh](utility_scripts/ubuntu/fix_nonsudo_access_to_ports.sh) just simple script adding you to dialout group so you can
  access comports without using sudo or sacrificing your child, also reboots your device, so if you are not interested in rebooting, simply copy
  the one line and than log out and log in.

  /helpers/ do not tread into this folder if you do not know what you are doing, which you do not since you are reading this file.

### Windows
  For windows purposes launch utility scripts as administrator
  [install_dependencies.bat](utility_scripts/ubuntu/install_dependencies.bat) gets you trough installing various stuff, it will not do this
  automatically as windows is even more retarded than ubuntu linux.


## Launcher scripts
There are currently just .sh launcher scripts.

### compile
Script is running the /protobuf_messages/parse_messsages script and than copies results into respective directories for every programming language.

### launch_
Launches process in a new terminal window, feel free to write process programm in other language and just replace the command in this shell file.

## configs
Shared config files for all languages using [TOML](https://toml.io/), pretty straightforward.


