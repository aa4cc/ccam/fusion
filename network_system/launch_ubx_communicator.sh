#!/bin/bash

# * Name:   launch_ubx_communicator.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

# * There is no need to launch exclusively python, you can change the file to run another script, without anyone from the outside noticing.

gnome-terminal -- sh -c "bash -c \"cd network_system; python3 python_scripts/ubx_communicator.py; exec bash\""