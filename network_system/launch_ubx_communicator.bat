:: * Name:   launch_ubx_communicator.bat
:: * Author: Matej Kriz, krizmat3@fel.cvut.cz
:: * Date:   2022_10_19

start cmd.exe @cmd /k "cd %~dp0&python .\python_scripts\ubx_communicator.py"