#include "trdp_communicator.hpp"
#include <thread>

int main (int argc, char** argv) {
    /* Init our communicator */
    trdpCommunicator communicator = trdpCommunicator();

    /* Fill necessary infromation into PORT_T structure */
    PORT_T source;
    uint8_t ipArray[4] = {10, 181, 100, 38}; // Array with our ip address
    uint32_t ipNum = IParray2IPnum(ipArray); // Convert array to uint
    source.srcIP = ipNum; // Set our IP as source IP, this is optional for receiver and can be set to 0
    source.secSrcIP = 0; // Secondary source is not used, otherwise fill this value in the same way as srcIP
    source.destIP = ipNum; // Set destination IP to join
    source.comId = 0; // Set com ID

    /* TRDP PD data are sent periodcally. We need to set cycle interval. 
     * Data will be automatically set with this period it is up to user to update data 
     * until this time. If data are not updated, old data will be used. */
    uint32_t interval_us = 1000000; // interval is alwayas defined in us

    /* Register our new publisher and get current time so we can update data in time.*/
    auto wake_time = std::chrono::system_clock::now();
    if (communicator.registerNewPublisher(source, interval_us, 128) > 0) {
        std::cout << "Registering new PD subscription failed" << std::endl;
        return 1;
    }

    uint32_t cnt = 0;

    /* Main loop updating data in outgoing telegram */
    while (true) {
        /* Prepare telegram */
        MSG_T msg;
        msg.port = source; // Identification of port
        uint8_t buffer[128]; // Our buffer where we will store outgoing data
        memset(buffer, 0, 128);
        uint32_t data_written = sprintf((char *) buffer, "Counter: %09d", cnt++);
        msg.data = buffer; // Update structure. Buffer is copied internally so user is responsible for possible leakage.
        msg.dataSize = data_written;
        
        /* Send telegram */
        communicator.send(msg);
        
        /* Wait until next telegram should be sent */
        wake_time += std::chrono::microseconds(interval_us);
        std::this_thread::sleep_until(wake_time);
    }
}