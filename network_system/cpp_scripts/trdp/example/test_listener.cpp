#include "trdp_communicator.hpp"
#include <thread>

int main (int argc, char** argv) {
    /* Init our communicator */
    trdpCommunicator communicator = trdpCommunicator();

    /* Fill necessary infromation into PORT_T structure */
    PORT_T source;
    uint8_t ipArray[4] = {10, 181, 100, 38}; // Array with our ip address
    uint32_t ipNum = IParray2IPnum(ipArray); // Convert array to uint
    source.srcIP = ipNum; // Set our IP as source IP, this is optional for receiver and can be set to 0
    source.secSrcIP = 0; // Secondary source is not used, otherwise fill this value in the same way as srcIP
    source.destIP = ipNum; // Set destination IP to join
    source.comId = 0; // Set com ID

    /* Register new PD subscription */
    if (communicator.registerNewPDSubscription(source) > 0) {
        std::cout << "Registering new PD subscription failed" << std::endl;
        return 1;
    }

    /* Try to receive data five times with cycle period greater than sender cycle */
    for (int i = 0; i < 5; i++) {
        /* Prepare MSG_T structure to receive data */
        MSG_T msg;
        msg.port = source; // Identification of port
        uint8_t buffer[128]; // Our buffer
        memset(buffer, 0, 128); // Clear buffer
        msg.data = buffer; // Update structure
        msg.dataSize = 128;

        /* try to receive data and print output */
        uint32_t dataRead = communicator.readPort(msg); // read data, msg.data buffer will be updated
        if (dataRead > 0 ) { // check if data were received
            printf("PD received - bytes: "); // Print data
            for (int i = 0; i < dataRead; i++)
                printf("%02hhx ", buffer[i]);
            printf("\n");
            printf("PD received - string: %s\n", buffer);
        }
        std::this_thread::sleep_for(std::chrono::seconds(2)); // Sleep, every second telegram should be missed
    }

    /* Do the same as in previous loop but with shorter cycle time */
    while (true) {
        MSG_T msg;
        msg.port = source;
        uint8_t buffer[128];
        memset(buffer, 0, 128);
        msg.data = buffer;
        msg.dataSize = 128;

        uint32_t dataRead = communicator.readPort(msg);
        
        if (dataRead > 0 ) {
            printf("PD received - bytes: ");
            for (int i = 0; i < dataRead; i++)
                printf("%02hhx ", buffer[i]);
            printf("\n");
            printf("PD received - string: %s\n", buffer);
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(500)); // Half of the sender cycle time, every telegram will be received twice
    }
}