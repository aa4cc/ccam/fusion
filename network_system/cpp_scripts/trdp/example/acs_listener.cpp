#include "trdp_communicator.hpp"
#include <thread>

#include <ecal/ecal.h>
#include <ecal/msg/protobuf/publisher.h>

#include "messages.pb.h"

#include <iostream>
#include <fstream>
#include <chrono>
#include <chrono>
#include <cstdint>

struct TimeDate64
{
  uint32_t secs;                      ///< elapsed time in seconds since 1970, January 1st, 00:00
  uint16_t ticks;                     ///< additional number of ticks (1 tick =1/65536 s)
  uint16_t rsv;                       ///< alignment
};

struct ACS_to_TCN_PC
{
  float speed_FDI;              ///< Speed calculated from digital inputs from odometry
  uint32_t teethCountAbsolute;  ///< Absolute odometry teeth counter
  int16_t speed_VCU;            ///< Reference speed obtained from VCU
  uint16_t teethCountRelative;  ///< Number of teeth counted from the last message
  uint16_t weight;              ///< Current weight of the tram 
  uint16_t reserved;            ///< Alignment
  int32_t force;                ///< Current traction or braking force
  TimeDate64 timestamp;         ///< Timestamp of the message 
};

int main (int argc, char** argv) {
  trdpCommunicator communicator = trdpCommunicator();

  eCAL::Initialize(argc, argv, "TRDP Odometry");
  eCAL::protobuf::CPublisher<MessageOdometry> publisher("odometry");

  PORT_T source;
  uint8_t ipArraySrc[4] = {192, 168, 10, 100};
  uint8_t ipArrayDest[4] = {192, 168, 10, 101};
  source.srcIP = IParray2IPnum(ipArraySrc);
  source.secSrcIP = 0;
  source.destIP = IParray2IPnum(ipArrayDest);
  source.comId = 20500;

  uint32_t cycle_us = 20000;
  size_t dataSize = sizeof(ACS_to_TCN_PC);

  if (communicator.registerNewPDSubscription(source) > 0) {
    std::cout << "Registering new PD subscription failed" << std::endl;
  }
  auto wake_time = std::chrono::system_clock::now();
  std::ofstream logfile;
  logfile.open("trdp_odometry.csv", std::ios::app);
  logfile << "timestamp_ns, "<< "speed_FDI, " << "teethCountAbsolute, " << "speed_VCU, " << "teethCountRelative, ";
  logfile << "weight, " << "reserved, " << "force, " << "timestamp_rsv, " << "timestamp_secs, " << "timestamp_ticks" << "\n";

  while (true) {
    MSG_T msg;
    msg.port = source;
    uint8_t buffer[dataSize];
    memset(buffer, 0, dataSize);
    msg.data = buffer;
    msg.dataSize = dataSize;
    uint32_t dataRead = communicator.readPort(msg);

    if (dataRead > 0 ) {
      ACS_to_TCN_PC received = *((ACS_to_TCN_PC*) msg.data);

      MessageOdometry proto_message;
      proto_message.set_ticks((uint64_t)received.teethCountRelative);
      publisher.Send(proto_message);

      logfile << std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count() << ", ";
      logfile << received.speed_FDI << ", ";
      logfile << received.teethCountAbsolute << ", ";
      logfile << received.speed_VCU << ", ";
      logfile << received.teethCountRelative << ", ";
      logfile << received.weight << ", ";            
      logfile << received.reserved << ", ";      
      logfile << received.force << ", ";              
      logfile << received.timestamp.rsv << ", ";
      logfile << received.timestamp.secs << ", ";
      logfile << received.timestamp.ticks;
      logfile << "\n";
    }
    wake_time += std::chrono::microseconds(cycle_us);
    std::this_thread::sleep_until(wake_time);
    }
}