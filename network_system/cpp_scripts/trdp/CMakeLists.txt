cmake_minimum_required(VERSION 3.16)
project(trdp-communicator)

find_package(eCAL REQUIRED)
find_package(Protobuf REQUIRED)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_BUILD_TYPE Debug)
include_directories ("compiled_libtrdp/")
include_directories ("src/")
include_directories ("src/api/trdp/")
include_directories ("src/api/vos/")
include_directories ("example/")
include_directories ("proto/")

add_executable(acs_listener example/acs_listener.cpp)
target_link_libraries(acs_listener trdpcommunicator)


set(files_proto
    ${CMAKE_CURRENT_SOURCE_DIR}/proto/messages.proto
)

PROTOBUF_TARGET_CPP(acs_listener ${CMAKE_CURRENT_SOURCE_DIR}/proto ${files_proto})


add_library(trdpcommunicator src/trdp_communicator)
target_link_libraries(acs_listener ${CMAKE_SOURCE_DIR}/compiled_libtrdp/libtrdpap.a pthread uuid)

target_link_libraries(acs_listener
  eCAL::core
  protobuf::libprotobuf
)
