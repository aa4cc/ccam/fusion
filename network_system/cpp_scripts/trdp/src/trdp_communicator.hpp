#include <iostream>
#include <cstdlib>
#include <string>
#include <map>
#include <queue>
#include <mutex>
#include <functional>
#include <cstring>
#include <thread>

#include "trdp_if_light.h"
#include "trdp_types.h"
#include "vos_thread.h"
#include "vos_utils.h"
#include "vos_sock.h"

typedef struct PORT_T {
    uint32_t srcIP;
    uint32_t secSrcIP;
    uint32_t destIP;
    uint32_t comId;

    bool operator<(const PORT_T& rhs) const {
        if (srcIP < rhs.srcIP)
            return true;
        if (srcIP > rhs.srcIP)
            return false;
        if (comId < rhs.comId)
            return true;
        return false;
    }
} PORT_T;

typedef struct {
    PORT_T port;
    uint8_t * data;
    size_t dataSize;
} MSG_T;

/// Helper function converting ip address stored in array to uint32
/// @param ip_array Array of size 4 specifing ip address
/// @return Converted value in uint32
uint32_t IParray2IPnum(uint8_t * ip_array);

/// Helper function converting ip address from uint32 into array of bytes.
/// @param ip_num IP address in form of uint32.
/// @param ip_array Pointer to the array of bytes where result will be stored.
void IPnum2IParray(uint32_t ip_num, uint8_t * ip_array);

class trdpCommunicator {
public:
    /// Base contructor of trdpCommunicator.
    trdpCommunicator();

    /// Destructor
    ~trdpCommunicator();

    /// Configuration loader. Uses standard form of cfg file.
    // loadCfg();

    /// Register new PD subscription for given IP and comID.
    /// @param port Struct defining port to listen to.
    int registerNewPDSubscription(PORT_T port);
    
    /// Register new MD subscription for given IP and comID.
    /// @param port Struct defining port to listen to.
    /// @param observeComId Set True if comId shall be observed.
    int registerNewMDSubscription(PORT_T port, bool observeComId);

    /// Register new PD or MD publisher for given IP and comID.
    /// @param port Struct defining port to publish to.
    /// @param interval Cycle time for PD msg.
    /// @param size Size of data packet.
    int registerNewPublisher(PORT_T port, uint32_t interval, uint32_t size);

    /// Read message from message queue (FIFO) for the given port. If subscribtion for the given port was
    /// not initialized before, 1 is returned.  If queue is empty, 2 is returned. If everything is ok, 
    /// 0 is returned. User is resposible for deleting allocated space.
    /// @param msg Message struct with filled port, allocated buffer and its size.
    /// @retval Size of data received.
    uint32_t readPort(MSG_T &msg);

    /// Updates internal buffer and pusblihes new data in next cycle.
    /// @param msg Msg to be sent.
    void send(MSG_T msg);
    
protected:
    // void MDCallback_ (void *pRefCon, TRDP_SESSION * appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize);
    // void PDCallback_ (void *pRefCon, TRDP_SESSION * appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize);

private:
    /// Main loop of PD receiver thread.
    void * PDReceiver();
    /// Main loop of MD receiver thread.
    void * MDReceiver();
    /// Main loop of sender thread.
    void * sender();

    TRDP_APP_SESSION_T appHandle; ///< TCNOpen session handle.
    std::map<PORT_T, TRDP_SUB_T*> subHandles; ///< TCNOpen PD subscription handles.
    std::map<PORT_T, TRDP_LIS_T> lisHandles; ///< TCNOpen MD listener handles.
    std::map<PORT_T, TRDP_PUB_T> pubHandles; ///< TCNOpen sender handle.
    TRDP_PD_CONFIG_T pdConfiguration = {NULL, NULL, TRDP_PD_DEFAULT_SEND_PARAM, TRDP_FLAGS_CALLBACK, 10000u, TRDP_TO_SET_TO_ZERO, 0u}; ///< TRDP PD configuration
    TRDP_MD_CONFIG_T mdConfiguration = {NULL, NULL, TRDP_MD_DEFAULT_SEND_PARAM, TRDP_FLAGS_CALLBACK, 0u, 0u, 0u, 0u, 0u, 0u, 0u}; ///< TRDP MD configuration
    TRDP_MEM_CONFIG_T dynamicConfig   = {NULL, 128000, {0}};
    TRDP_PROCESS_CONFIG_T processConfig = {"Me", "", "", TRDP_PROCESS_DEFAULT_CYCLE_TIME, 0u, TRDP_OPTION_BLOCK}; ///< TCNOpne process config.
    // VOS_THREAD_T rcvThread, sndThread, mdThread; ///< Threads receiving PD, MD and sending data.
    std::thread rcvThread, sndThread, mdThread; 

    // std::map<PORT_T, std::queue<MSG_T>> PDMsgQueues; ///< PD msg queues paired with respective port. All received PDs are stored here.
    // std::map<PORT_T, std::mutex> PDMsgQueuesMtx; ///< PD msg queue mutexes.
    // std::map<PORT_T, std::queue<MSG_T>> MDMsgQueues; ///< MD msg queues paired with respective port. All received PDs are stored here.
    // std::map<PORT_T, std::mutex> MDMsgQueuesMtx; ///< MD msg queue mutexes.
    // std::queue<MSG_T> sendMsgQueue; ///< Queue for outgoing messages.
    // std::mutex sendMsgQueueMtx; ///< Mutex protecting sendMsgQueue.
    // std::condition_variable sendSignal; ///< Signal variable for sending Msgs.

    // std::function<void (void*, TRDP_SESSION*, const TRDP_PD_INFO_T*, UINT8*, UINT32)> PDReceiveCB;
    // std::function<void (void*, TRDP_SESSION*, const TRDP_PD_INFO_T*, UINT8*, UINT32)> MDReceiveCB; // Callback functions.
    
    bool initialized = false;

};