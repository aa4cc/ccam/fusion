#include "trdp_communicator.hpp"

/* Some sample comId definitions    */

/* Expect receiving AND Send as echo    */
#define PD_COMID1               2001
#define PD_COMID1_CYCLE         50000
#define PD_COMID1_TIMEOUT       150000
#define PD_COMID1_DATA_SIZE     32

#define EXIT_OK 0
#define EXIT_FAIL 1

using namespace std::chrono_literals;

uint32_t IParray2IPnum(uint8_t * ip_array) {
    uint32_t out_IP = 0;
    out_IP |= (uint32_t) ip_array[0] << 24; 
    out_IP |= (uint32_t) ip_array[1] << 16; 
    out_IP |= (uint32_t) ip_array[2] << 8; 
    out_IP |= (uint32_t) ip_array[3];
    return out_IP;
}

void IPnum2IParray(uint32_t ip_num, uint8_t * ip_array) {
    ip_array[0] = (uint8_t) ip_num >> 24;
    ip_array[1] = (uint8_t) ip_num >> 16;
    ip_array[2] = (uint8_t) ip_num >> 8;
    ip_array[3] = (uint8_t) ip_num;
}

/// Callback routine for TRDP logging/error output
/// @param[in]        pRefCon          user supplied context pointer
/// @param[in]        category         Log category (Error, Warning, Info etc.)
/// @param[in]        pTime            pointer to NULL-terminated string of time stamp
/// @param[in]        pFile            pointer to NULL-terminated string of source module
/// @param[in]        LineNumber       line
/// @param[in]        pMsgStr          pointer to NULL-terminated string
/// @retval         none
void dbgOut (
    void        *pRefCon,
    TRDP_LOG_T  category,
    const CHAR8 *pTime,
    const CHAR8 *pFile,
    UINT16      LineNumber,
    const CHAR8 *pMsgStr) {
    const char *catStr[] = {"**Error:", "Warning:", "   Info:", "  Debug:", "   User:"};
    printf("%s %s %s:%d %s",
           pTime,
           catStr[category],
           pFile,
           LineNumber,
           pMsgStr);
}

trdpCommunicator::trdpCommunicator() {
    if (tlc_init(dbgOut, NULL, &dynamicConfig) != TRDP_NO_ERR) {
        printf("Initialization error\n");
        return;
    }

    if (tlc_openSession(&appHandle, 0u, 0, NULL, &pdConfiguration, NULL, &processConfig) != TRDP_NO_ERR) {
        vos_printLogStr(VOS_LOG_USR, "Initialization error\n");
        return;
    }
    
    // PDReceiveCB = std::bind(&trdpCommunicator::PDCallback_, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    // PDReceiveCB = [this] (void *pRefCon, TRDP_SESSION * appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize) {
    //     this->PDCallback_(pRefCon, appHandle, pMsg, pData, dataSize);
    // };
    // MDReceiveCB = [this] (void *pRefCon, TRDP_SESSION * appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize) {
    //     this->MDCallback_(pRefCon, appHandle, pMsg, pData, dataSize);
    // };
    // MDReceiveCB = std::bind(&trdpCommunicator::MDCallback_, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);

    rcvThread = std::thread(std::bind(&trdpCommunicator::PDReceiver, this));
    rcvThread.detach();

    mdThread = std::thread(std::bind(&trdpCommunicator::MDReceiver, this));
    mdThread.detach();

    sndThread = std::thread(std::bind(&trdpCommunicator::sender, this));
    sndThread.detach();

    initialized = true;
}

int trdpCommunicator::registerNewPDSubscription(PORT_T port) {
    // subHandles.insert(std::pair<PORT_T, TRDP_SUB_T>(port, ))
    subHandles[port] = new TRDP_SUB_T;
    TRDP_ERR_T err = tlp_subscribe(appHandle, (TRDP_SUB_T*) subHandles[port], NULL, NULL, 0u, port.comId, 0, 0, port.srcIP, port.secSrcIP, 
                                   port.destIP, TRDP_FLAGS_DEFAULT, NULL, 1000000u, TRDP_TO_SET_TO_ZERO);

    if (err != TRDP_NO_ERR) {
        vos_printLog(VOS_LOG_ERROR, "prep md receive error - comID %u, srcIP %u.%u.%u.%u, destIP %u.%u.%u.%u\n", port.comId,
        (uint8_t) port.srcIP >> 24, (uint8_t) port.srcIP >> 16, (uint8_t) port.srcIP >> 8, (uint8_t) port.srcIP,
        (uint8_t) port.destIP >> 24, (uint8_t) port.destIP >> 16, (uint8_t) port.destIP >> 8, (uint8_t) port.destIP);
        return EXIT_FAIL;
    }

    tlc_updateSession(appHandle);
    // PDMsgQueues[port] = std::queue<MSG_T>();
    return EXIT_OK;
}

int trdpCommunicator::registerNewMDSubscription(PORT_T port, bool observeComId) {
    TRDP_ERR_T err = tlm_addListener(appHandle, &lisHandles[port], NULL, NULL, observeComId, port.comId, 0, 0, port.srcIP, port.secSrcIP, 
                                   port.destIP, TRDP_FLAGS_DEFAULT, NULL, NULL);

    if (err != TRDP_NO_ERR) {
        vos_printLog(VOS_LOG_ERROR, "prep md receive error - comID %u, srcIP %u.%u.%u.%u, destIP %u.%u.%u.%u\n", port.comId,
        (uint8_t) port.srcIP >> 24, (uint8_t) port.srcIP >> 16, (uint8_t) port.srcIP >> 8, (uint8_t) port.srcIP,
        (uint8_t) port.destIP >> 24, (uint8_t) port.destIP >> 16, (uint8_t) port.destIP >> 8, (uint8_t) port.destIP);
        return EXIT_FAIL;
    }

    tlc_updateSession(appHandle);
    // MDMsgQueues[port] = std::queue<MSG_T>();
    return EXIT_OK;
}

int trdpCommunicator::registerNewPublisher(PORT_T port, uint32_t interval, uint32_t size) {
    TRDP_ERR_T err = tlp_publish(appHandle, &pubHandles[port], NULL, NULL, 0, port.comId, 0, 0, port.srcIP, port.destIP,
                                 interval, 0, TRDP_FLAGS_NONE, NULL, NULL, size);

    if (err != TRDP_NO_ERR) {
        vos_printLog(VOS_LOG_ERROR, "prep pub error - comID %u, srcIP %u.%u.%u.%u, destIP %u.%u.%u.%u\n", port.comId,
        (uint8_t) port.srcIP >> 24, (uint8_t) port.srcIP >> 16, (uint8_t) port.srcIP >> 8, (uint8_t) port.srcIP,
        (uint8_t) port.destIP >> 24, (uint8_t) port.destIP >> 16, (uint8_t) port.destIP >> 8, (uint8_t) port.destIP);
        return EXIT_FAIL;
    }

    tlc_updateSession(appHandle);
    return EXIT_OK;
}

void * trdpCommunicator::PDReceiver() {
    TRDP_APP_SESSION_T  sessionhandle = appHandle;
    TRDP_ERR_T      result;
    TRDP_TIME_T     interval = {0,0};
    TRDP_FDS_T      fileDesc;
    INT32           noDesc = 0;

    while (vos_threadDelay(0u) == VOS_NO_ERR)   /* this is a cancelation point! */
    {
        FD_ZERO(&fileDesc);
        result = tlp_getInterval(sessionhandle, &interval, &fileDesc, &noDesc);
        if (result != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_WARNING, "tlp_getInterval failed: %s\n", vos_getErrorString((VOS_ERR_T) result));
        }
        noDesc = vos_select(noDesc + 1, &fileDesc, NULL, NULL, &interval);
        result = tlp_processReceive(sessionhandle, &fileDesc, &noDesc);
        if ((result != TRDP_NO_ERR) && (result != TRDP_BLOCK_ERR))
        {
            vos_printLog(VOS_LOG_WARNING, "tlp_processReceive failed: %s\n", vos_getErrorString((VOS_ERR_T) result));
        }
    }
    return NULL;
}

void * trdpCommunicator::MDReceiver() {
    TRDP_APP_SESSION_T  sessionhandle = appHandle;
    TRDP_ERR_T      result;
    TRDP_TIME_T     interval = {0,0};
    TRDP_FDS_T      fileDesc;
    INT32           noDesc = 0;

    while (vos_threadDelay(0u) == VOS_NO_ERR)   /* this is a cancelation point! */
    {
        FD_ZERO(&fileDesc);
        result = tlm_getInterval(sessionhandle, &interval, &fileDesc, &noDesc);
        if (result != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_WARNING, "tlm_getInterval failed: %s\n", vos_getErrorString((VOS_ERR_T) result));
        }
        noDesc = vos_select(noDesc + 1, &fileDesc, NULL, NULL, &interval);
        result = tlm_process(sessionhandle, &fileDesc, &noDesc);
        if ((result != TRDP_NO_ERR) && (result != TRDP_BLOCK_ERR))
        {
            vos_printLog(VOS_LOG_WARNING, "tlm_process failed: %s\n", vos_getErrorString((VOS_ERR_T) result));
        }
    }
    return NULL;
}

void * trdpCommunicator::sender() {
    TRDP_APP_SESSION_T  sessionhandle = appHandle;
    while(vos_threadDelay(1000u) == VOS_NO_ERR){
        TRDP_ERR_T result = tlp_processSend(sessionhandle);

        if ((result != TRDP_NO_ERR) && (result != TRDP_BLOCK_ERR))
            vos_printLog(VOS_LOG_WARNING, "tlp_processSend failed: %s\n", vos_getErrorString((VOS_ERR_T) result));

    }
    return NULL;
}

void trdpCommunicator::send(MSG_T msg) {
    if (pubHandles.contains(msg.port))
        tlp_put(appHandle, pubHandles[msg.port], msg.data, msg.dataSize);
}

trdpCommunicator::~trdpCommunicator() {
    sndThread.join();
    mdThread.join();
    rcvThread.join();
    for(auto pubHandle : pubHandles)
        tlp_unpublish(appHandle, pubHandle.second);
    for (auto subHandle : subHandles) {
        tlp_unsubscribe(appHandle, *subHandle.second);
        delete subHandle.second;
    }
    tlc_terminate();
}

// void trdpCommunicator::PDCallback_ (void *pRefCon, TRDP_SESSION * appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize) {
//     /*    Check why we have been called    */
//     std::cout << "Callback called" << std::endl;
//     if(pMsg->resultCode == TRDP_NO_ERR) {
//         vos_printLog(VOS_LOG_USR, "> ComID %d received\n", pMsg->comId);
//         MSG_T in_msg;

//         in_msg.port.srcIP = pMsg->srcIpAddr;
//         in_msg.port.secSrcIP = 0;
//         in_msg.port.destIP = pMsg->destIpAddr;
//         in_msg.port.comId = pMsg->comId;
//         in_msg.dataSize = dataSize;
//         in_msg.data = new uint8_t[dataSize];
//         if (pData) {
//             std::memcpy(in_msg.data, pData, dataSize);
//         }

//         std::lock_guard<std::mutex> lock(PDMsgQueuesMtx[in_msg.port]);
//         PDMsgQueues[in_msg.port].push(in_msg);
//     } else if (pMsg->resultCode == TRDP_TIMEOUT_ERR) {
//         /* The application can decide here if old data shall be invalidated or kept    */
//         vos_printLog(VOS_LOG_USR, "> Packet timed out (ComID %d, SrcIP: %s)\n",
//                      pMsg->comId,
//                      vos_ipDotted(pMsg->srcIpAddr));
//     } else {
//         vos_printLog(VOS_LOG_USR, "> Error on packet received (ComID %d), err = %d\n",
//                      pMsg->comId,
//                      pMsg->resultCode);
//     }
// }

// void trdpCommunicator::MDCallback_ (void *pRefCon, TRDP_SESSION * appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize) {
//     /*    Check why we have been called    */
//     if(pMsg->resultCode == TRDP_NO_ERR) {
//         vos_printLog(VOS_LOG_USR, "> ComID %d received\n", pMsg->comId);
//         MSG_T in_msg;

//         in_msg.port.srcIP = pMsg->srcIpAddr;
//         in_msg.port.secSrcIP = 0;
//         in_msg.port.destIP = pMsg->destIpAddr;
//         in_msg.port.comId = pMsg->comId;
//         in_msg.dataSize = dataSize;
//         in_msg.data = new uint8_t[dataSize];
//         if (pData) {
//             std::memcpy(in_msg.data, pData, dataSize);
//         }

//         std::lock_guard<std::mutex> lock(MDMsgQueuesMtx[in_msg.port]);
//         MDMsgQueues[in_msg.port].push(in_msg);
//     } else if (pMsg->resultCode == TRDP_TIMEOUT_ERR) {
//         /* The application can decide here if old data shall be invalidated or kept    */
//         vos_printLog(VOS_LOG_USR, "> Packet timed out (ComID %d, SrcIP: %s)\n",
//                      pMsg->comId,
//                      vos_ipDotted(pMsg->srcIpAddr));
//     } else {
//         vos_printLog(VOS_LOG_USR, "> Error on packet received (ComID %d), err = %d\n",
//                      pMsg->comId,
//                      pMsg->resultCode);
//     }
// }

uint32_t trdpCommunicator::readPort(MSG_T& msg) {
    uint32_t ret = 0;
    TRDP_PD_INFO_T info;
    // std::cout << "Subhandles size: " << subHandles.size() << std::endl;
    if (subHandles.contains(msg.port)) {
        uint32_t dataRead = msg.dataSize;
        tlp_get(appHandle, *subHandles[msg.port], &info, msg.data, &dataRead);
        std::cout << "Data read: " << dataRead << "Error: " << vos_getErrorString((VOS_ERR_T) info.resultCode) << std::endl;
        if (info.resultCode == TRDP_NO_ERR)
            ret = dataRead;
    }

    // if (PDMsgQueues.contains(port)) {
    //     if (!PDMsgQueues[port].empty()) {
    //         *msg = PDMsgQueues[port].front();
    //         PDMsgQueues[port].pop();
    //         ret = 0;
    //     }
    //     ret = 1;
    // } else if (MDMsgQueues.contains(port)) {
    //     if (!PDMsgQueues[port].empty()) {
    //         *msg = MDMsgQueues[port].front();
    //         MDMsgQueues[port].pop();
    //         ret = 0;
    //     }
    //     ret = 1;
    // } else {
    //     ret = 2;
    // }
    return ret;
}