#!/bin/bash

# * Name:   fix_nonsudo_access_to_ports.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

read -p "For this, there is REBOOT needed. Are you sure you want to continue? (y/n) " CONT
if [ "$CONT" = "y" ]; then
  sudo usermod -a -G dialout $USER
  sudo reboot
fi
