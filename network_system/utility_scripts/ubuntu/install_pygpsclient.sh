#!/bin/bash

# * Name:   install_pygpsclient.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

chmod u+x ./helpers/check_python.sh

# checks if sufficient python version is installed
PYTHON_REF=$(source ./helpers/check_python.sh)
if [[ "$PYTHON_REF" == "NoPython" ]]; then
    echo "Python3.10+ is not installed."
    exit
fi

echo "Installing PIP3..."
sudo apt install python3-pip

echo "Installing Tkinter..."
sudo apt install python3-tk
sudo pip3 install tk

echo "Installing PyGPSClient..."
sudo pip3 install pyubx2 pygnssutils
sudo pip3 install PyGPSClient