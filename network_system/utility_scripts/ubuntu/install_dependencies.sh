#!/bin/bash

# * Name:   install_dependencies.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

chmod u+x ./helpers/check_python.sh

# checks if sufficient python version is installed
PYTHON_REF=$(source ./helpers/check_python.sh)
if [[ "$PYTHON_REF" == "NoPython" ]]; then
    echo "Python3.10+ is not installed."
    exit
fi
echo "Installing PIP3..."
sudo apt install python3-pip

echo "Installing required PIP packages for ubx communication..."
sudo pip3 install pyubx2 pygnssutils

echo "Installing required PIP packages for toml parsing..."
sudo pip3 install toml

echo "Installing eCAL repository and program..."
sudo add-apt-repository ppa:ecal/ecal-latest
sudo apt-get update
sudo apt-get install ecal

echo "Installing Python3 - eCAL integration module..."
sudo apt install python3-ecal5

echo "Installing protobuf compiler..."
sudo apt install protobuf-complier

echo "Installing cmake..."
sudo apt install cmake

echo "Installing trdp communication dependencies..."
sudo apt install uuid-dev
