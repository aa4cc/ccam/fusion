#!/bin/bash

# * Name:   fix_obd_port_not_displaying.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

for f in /usr/lib/udev/rules.d/*brltty*.rules; do
sudo ln -s /dev/null "/etc/udev/rules.d/$(basename "$f")"
done
sudo udevadm control --reload-rules

sudo systemctl mask brltty.path
