:: * Name:   install_dependencies.bat
:: * Author: Matej Kriz, krizmat3@fel.cvut.cz
:: * Date:   2022_10_19

@ECHO OFF
echo "Check with 'python --version' that you have python 3.10 and higher please."
echo "Furthermore check you have pip installed by  'pip --version'."
pause


echo "Installing required PIP packages for ubx communication..."
pip install pyubx2 pygnssutils

echo "Installing required PIP packages for toml parsing..."
pip install toml

echo "Please install eCAL and Python integration module from https://eclipse-ecal.github.io/ecal/_download_archive/download_archive_ecal_5_10_2.html"
pause

echo "Installing protobuf compiler..."
sudo apt install protobuf-complier