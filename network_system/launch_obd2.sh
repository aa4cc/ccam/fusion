#!/bin/bash

# * Name:   launch_obd2.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

# * There is no need to launch exclusively python, you can change the file to run another script, without anyone from the outside noticing.

gnome-terminal -- sh -c "bash -c \"cd network_system; python3 python_scripts/obd2.py; exec bash\""