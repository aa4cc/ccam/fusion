#!/bin/bash

# * Name:   launch_ntrip.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_19

# * There is no need to launch exclusively python, you can change the file to run another script, without anyone from the outside noticing.

gnome-terminal -- sh -c "bash -c \"cd network_system; python3 python_scripts/ntrip.py; exec bash\""