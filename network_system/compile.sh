#!/bin/bash

# * Name:   compile.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd "$SCRIPTPATH"

# * compile messages
./protobuf_messages/parse_messages.sh

# * moving python message file
mv ./protobuf_messages/messages_pb2.py ./python_scripts/messages_pb2.py