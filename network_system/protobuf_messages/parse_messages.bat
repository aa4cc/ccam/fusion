:: * Name:   parse_messages.bat
:: * Author: Matej Kriz, krizmat3@fel.cvut.cz
:: * Date:   2022_10_19

@ECHO OFF
%~dp0protobuf\protoc-21.8-win64\bin\protoc.exe --proto_path=%~dp0 --python_out=%~dp0 %~dp0messages.proto
