#<timeMS>, <accelX>, <accelY>, <accelZ>, <gyroX>, <gyroY>, <gyroZ>, <magX>, <magY>, <magZ>
#send the following char to the IMU to change settings, it should remember it even after restart:
"""
SPACE) -- Pause/resume serial port printing
t -- Turn time readings on or off
a -- Turn accelerometer readings on or off
g -- Turn gyroscope readings on or off
m -- Turn magnetometer readings on or off
c -- Switch to/from calculated values from/to raw readings
q -- Turn quaternion readings on or off (qw, qx, qy, and qz are printed after mag readings)
e -- Turn Euler angle calculations (pitch, roll, yaw) on or off (printed after quaternions)
h -- Turn heading readings on or off
r -- Adjust log rate in 10Hz increments between 1-100Hz (1, 10, 20, ..., 100)
A -- Adjust accelerometer full-scale range. Cycles between ± 2, 4, 8, and 16g.
G -- Adjust gyroscope full-scale range. Cycles between ± 250, 500, 1000, 2000 dps.
s -- Enable/disable SD card logging
"""


IMU_PORT = "/dev/ttyACM2" #edit this


import sys
import serial
import threading
import time

from messages_pb2 import MessageImu

import ecal.core.core as ecal_core
from ecal.core.publisher import ProtoPublisher
#from ecal.core.subscriber import ProtoSubscriber


class IMU_reader():
    def __init__(self, port, log_file):
        ecal_core.initialize(sys.argv, "imu_communicator")
        #ecal_core.set_process_state(1, 1, "UBX communicator process")
        self.ecal_pub = ProtoPublisher("imu", MessageImu)
        self.port = port
        self.log_file = log_file
        self.open = False
        self.t = None

    def read_imu_data(self, verbose):
        f = open(self.log_file, 'a')
        f.write("STAR\n")
        imu_port = serial.Serial(port=self.port, baudrate=115200, timeout=1)
        data = b'\n'
        msg = MessageImu()
        while self.open:
            if imu_port.in_waiting:
                data = imu_port.readline()
                f.write(str(time.time_ns())+",")
                data = data.split(b',')
                msg.time_ms = int(data[0])
                msg.accel_x = int(data[1])
                msg.accel_y = int(data[2])
                msg.accel_z = int(data[3])
                msg.gyro_x = int(data[4])
                msg.gyro_y = int(data[5])
                msg.gyro_z = int(data[6])
                msg.mag_x = int(data[7])
                msg.mag_y = int(data[8])
                msg.mag_z = int(data[9])
                self.ecal_pub.send(msg)
                for x in data:
                    f.write(str(int(x)) + ",")                   
                f.write("\n")
                if verbose:
                    print(data)
        f.close()
        

    def run(self, verbose=False):
        self.open = True
        print("starting imu thread")
        self.t = threading.Thread(target=self.read_imu_data, args=(verbose,))
        self.t.start()
        print(self.t)

    def close(self):
        self.open = False
        self.t.join()


if __name__ =="__main__":
    imu = IMU_reader(IMU_PORT, "imu/imu.csv")
    imu.run(verbose=False)
    while True:
        a = input()
        if a == 'q':
            break
    imu.close()