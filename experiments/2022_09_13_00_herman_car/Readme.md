# Jízda autem po Praze s Ivem Hermanem 13. září 2022

## Účastníci experimentu / Measured By 
Jakub Kašpar, Ivo Herman, Zdeněk Hurák

## Vůz a instrumentace / Vehicle And Sensors Used 
- Auto Iva Hermana: Ford Galaxy r.v. 2019.
- Palubní měřicí a komunikační jednotka Hermanů: UCU 5.0-N. 
- [uBlox ZED-F9R](https://www.u-blox.com/en/product/zed-f9r-module) s předplacenou službou [CZEPOS RTK3-MSM](http://czepos.cuzk.cz/_servicesProducts.aspx) od Zeměměřičského ústavu.
- Konektor OBD-II, jmenovitě [ELM327 OBDII USB V1.5](https://www.hdkamery.cz/autodiagnostika-elm327-obdii-usb-v1-5-pc-touchscan-cz/).
- the datalogging script is in tramtest folder
- ... 

## Trasa
Z kampusu na Karlově náměstí po nábřeží (Masarykově a pak Smetanově) přes Palachovo náměstí, Klárov, Chotkovy sady, Hradčanskou, Svatovítskou, Jugoslávských partyzánů, Roztoky, Levý Hradec (zastávka), Velké Přílepy, Suchdol (zastávka), Horoměřice, Bořislavka, Vítězné náměstí, Svatovítská, tunel Blanka, kampus na Karlově náměstí.
<br/>
Coordinates measured by ublox module can be visualized [here](https://www.google.com/maps/d/edit?mid=18mGzSI4iZ94PcLGWZb4TbmoGAnHzirc&usp=sharing) 

## Problémy / Problems
### Problems with ublox zed f9r
- It seems that when RTK and deadreckogning are on, you cant run navigation on more than 10 Hz. Even at 10 Hz it sometimes falls back to 1 Hz for a couple of seconds. I havent figured out why that happens, probably the chip is not fast enough. Reducing the number of messages to (send nav-pvt only) didnt work either. <br/>
- note for anyone using the tram_0_1.py script: the automatic switch of message rate after sensors were calibrated didnt work so I commented it out

