There are 3 series of data - usek1, usek2, usek3

in each series there are 4 csv files - one for each unit we were using:
	onboard units:
		-quectel
		-ubx, 
	our unit: 
		-ubx-zed-f9r + deadreckogning + RTK
 	odometry unit:
		-read from car-computer via OBDII (1kmh resolution)

the csv2timetable.mlx matlab live script loads the data from the series specified in the "USEK" variable into datatables and converts the odometry data from km/h to m/s.

Time is saved in the DateGPS column, at the end of csv2timetable.mlx there is an example how to convert time from matlab's datetime format into seconds.