#<UBX(NAV-PVT, iTOW=07:28:20, year=2022, month=11, day=14, hour=7, min=28, second=20, validDate=1, validTime=1, fullyResolved=1, validMag=1, tAcc=24, nano=275296, fixType=3, gnssFixOk=1, difSoln=0, psmState=0, headVehValid=0, carrSoln=0, confirmedAvai=1, confirmedDate=1, confirmedTime=1, numSV=28, lon=14.5332018, lat=50.0720802, height=316663, hMSL=272389, hAcc=1323, vAcc=2000, velN=-3, velE=2, velD=-14, gSpeed=4, headMot=0.0, sAcc=186, headAcc=180.0, pDOP=0.96, invalidLlh=0, lastCorrectionAge=0, reserved0=1011181288, headVeh=0.0, magDec=4.5, magAcc=0.6)>
#{'_immutable': True, '_mode': 0, '_payload': b'p\x18\xc1\x06\xe6\x07\x0b\x0e\x07\x1c\x14?\x18\x00\x00\x00`3\x04\x00\x03\x01\xea\x1c2\x97\xa9\x08\xa2d\xd8\x1d\xf7\xd4\x04\x00\x05(\x04\x00+\x05\x00\x00\xd0\x07\x00\x00\xfd\xff\xff\xff\x02\x00\x00\x00\xf2\xff\xff\xff\x04\x00\x00\x00\x00\x00\x00\x00\xba\x00\x00\x00\x80\xa8\x12\x01`\x00\x00\x00\xe8fE<\x00\x00\x00\x00\xc2\x01<\x00', '_length': b'\\\x00', '_checksum': b'v\xe9', '_parsebf': True, '_scaling': True, '_ubxClass': b'\x01', '_ubxID': b'\x07', 'iTOW': 113318000, 'year': 2022, 'month': 11, 'day': 14, 'hour': 7, 'min': 28, 'second': 20, 'validDate': 1, 'validTime': 1, 'fullyResolved': 1, 'validMag': 1, 'tAcc': 24, 'nano': 275296, 'fixType': 3, 'gnssFixOk': 1, 'difSoln': 0, 'psmState': 0, 'headVehValid': 0, 'carrSoln': 0, 'confirmedAvai': 1, 'confirmedDate': 1, 'confirmedTime': 1, 'numSV': 28, 'lon': 14.5332018, 'lat': 50.0720802, 'height': 316663, 'hMSL': 272389, 'hAcc': 1323, 'vAcc': 2000, 'velN': -3, 'velE': 2, 'velD': -14, 'gSpeed': 4, 'headMot': 0.0, 'sAcc': 186, 'headAcc': 180.0, 'pDOP': 0.96, 'invalidLlh': 0, 'lastCorrectionAge': 0, 'reserved0': 1011181288, 'headVeh': 0.0, 'magDec': 4.5, 'magAcc': 0.6}
import pyubx2
import datetime
source = open("raw.csv", "r")
output = open("parsed.csv", "w")
attr_list = ['iTOW', 'year', 'month', 'day', 'hour', 'min', 'second', 'validDate', 'validTime', 'fullyResolved', 'validMag', 'tAcc', 'nano', 'fixType', 'gnssFixOk', 'difSoln', 'psmState', 'headVehValid', 'carrSoln', 'confirmedAvai', 'confirmedDate', 'confirmedTime', 'numSV', 'lon', 'lat', 'height', 'hMSL', 'hAcc', 'vAcc', 'velN', 'velE', 'velD', 'gSpeed', 'headMot', 'sAcc', 'headAcc', 'pDOP', 'invalidLlh', 'lastCorrectionAge', 'reserved0', 'headVeh', 'magDec', 'magAcc']
for line in source:
  line = line[:-1] # remove \n
  if line[0] == "t":
    output.write("human_readable_timestamp, timestamp_ns, device_name")
    for attr_name in attr_list:
      output.write(", " + attr_name )
    output.write("\n")
    
  else:
    line = line.split(", ")
    ubx = bytes.fromhex(line[2])
    message = pyubx2.UBXReader.parse(ubx)

    if hasattr(message, "lon"):
      output.write(str(datetime.datetime.fromtimestamp( int(line[0])/(10**9)) ) + ", ")
      output.write(line[0] + ', ')
      output.write(line[1])
      for attr_name in attr_list:
        output.write( ", " + str( getattr(message, attr_name) ) )
      output.write("\n")
    
