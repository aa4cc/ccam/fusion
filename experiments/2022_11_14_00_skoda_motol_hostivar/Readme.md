************************************************************************
*This is a template of how we would like to store data from experiments*
************************************************************************




Why .toml?
  Simple, and supported by all major languages (C, C++, C#, Java, Python, Matlab, Julia, Rust, Fortran, Ruby, Pascal,...).
  Less complicated than .json. In python it is simply loaded by 'import toml' for matlab there is matlab-toml extension.

For folder and file names everything written inside "{" and "}" will bw ignored, as it is used for human readability only,
just do not use any wierd characters or spaces for the name.

Main Folder:
  Names yyyy_mm_dd_XX where XX is experiment number of that date it inclides info.toml file containing name, date of creation
  (for use in the future) and names of participants with contact info. Also comment is preffered to specify any useful info about
  experiment.


Sensor Data:
  Data from sensors are stored under sensor_data/sensorXX/ in raw.data file for raw input and parsed data in csv file readable
  for nearly all languages including Matlab timetables.
  Data should be tagged by two equivalent timetags: human readable date-time and Unix timestamp increased to nanoseconds.
  If you do not need ns accuracy, simply multiply ms by 10^6 to get ns with tailing zeros, I assume no stramlined experiment will ever need
  higher accuracy than ns so it is intentionaly an overkill.

Complete Data:
  Stored under complete_data/yyyy_mm_dd_XX/ it includes info.toml file with data on date of completion creation and sensors used.
  It has the same structure as sensor parsed.csv but it is interpolated into desired frequency by, for example, Matlab timetable.
  Frequency and interpolation method should be stated in info.toml, also one should specify sensor data used.

Analysis:
  Other from specifiyng date and participants, analysis is a sandbox, one can use sensor data directly or not. But for sanity sake, try to 
  specify what you did and how, someone might find it useful.
