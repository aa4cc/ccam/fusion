:: * Name:   tram_0_3_win.bat
:: * Author: Matej Kriz, krizmat3@fel.cvut.cz
:: * Date:   2022_10_19

:: * compile system
call %~dp0network_system\compile.bat

:: * launch logger
call %~dp0network_system\launch_logger.bat

:: * launch obd2
call %~dp0network_system\launch_obd2.bat

:: * launch ubx_communicator
call %~dp0network_system\launch_ubx_communicator

:: * launch ntrip
call %~dp0network_system\launch_ntrip