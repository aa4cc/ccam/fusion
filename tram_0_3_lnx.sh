#!/bin/bash

# * Name:   tram_0_3_lnx.sh
# * Author: Matej Kriz, krizmat3@fel.cvut.cz
# * Date:   2022_10_15

# * compile system
chmod u+x ./network_system/compile.sh
./network_system/compile.sh

# * launch logger
chmod u+x ./network_system/launch_logger.sh
./network_system/launch_logger.sh

# * launch obd2
chmod u+x ./network_system/launch_obd2.sh
./network_system/launch_obd2.sh

# * launch ubx_communicator
chmod u+x ./network_system/launch_ubx_communicator.sh
./network_system/launch_ubx_communicator.sh

# * launch ntrip
chmod u+x ./network_system/launch_ntrip.sh
./network_system/launch_ntrip.sh