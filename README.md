# Real-time onboard model-based sensor fusion

Model-based fusion of GNSS data with measurements from IMU possibly from some other onboard sensors such as odometry, and data from a digital map to get an accurate position and velocity  estimate for the vehicle.

# Current version
Currently version 0.3 is used, hopefully staying for some time. [more info](/network_system/Readme.md)
