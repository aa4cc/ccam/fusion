# Created by Matej Kriz (krizmat3@fel.cvut.cz), 2022
# Czech TEchnical University in Prague, Faculty of Electrical Engineering

import serial

class PulserNucleo:
  def __init__(self, comport: str, baudrate: int = 115200) -> None:
    self.__period_us = 0
    self.__comport: str = comport
    self.__baudrate: int = baudrate
    self.__serial: serial.Serial

  def start_pulser(self) -> bool:
    self.__serial = serial.Serial(port=self.__comport, baudrate=self.__baudrate)
    return self.__serial.is_open()
  
  def stop_pulser(self) -> None:
    self.set_period(0)
    self.__serial.close()
  
  def set_period(self, period_us: int) -> None:
    if self.__period_us != period_us:
      self.__period_us = period_us

      self.__serial.write(self.__period_us.to_bytes(4, byteorder='big', signed=False))
      
  def set_frequency(self, frequency_hz: float) -> None:
    period_us: int = 0
    if frequency_hz > 0:
      period_us = round((10**9)/frequency_hz)
    self.set_period(period_us)

  def get_period_us(self) -> int:
    return self.__period_us

# Run for debug
if __name__ == "__main__":
  from time import sleep

  # Device specific init
  pulser: PulserNucleo = PulserNucleo('/dev/ttyACM1')

  print("DEBUG: Starting pulser.")
  start_ret: bool = pulser.start_pulser()

  print("Debug: Start of pulser was ", end='')
  if start_ret:
    print("SUCCESFUL.")
  else:
    print("UNSUCCESFUL.")
    exit(-1)
  
  del start_ret

  print("Debug: No output")
  print("       Should be outputting period of " + str(0) + ".")
  print("       Is outputting period of        " + str(pulser.get_period_us()) + ".")
  sleep(10)

  # Frequency test
  TEST_FREQUENCY: float = 2.0
  pulser.set_frequency(TEST_FREQUENCY)
  print("Debug: Frequency")
  print("       Should be outputting period of " + str(round((10**9)/TEST_FREQUENCY)) + ".")
  print("       Is outputting period of        " + str(pulser.get_period_us()) + ".")
  sleep(10)
  del TEST_FREQUENCY

  # Period test
  TEST_PERIOD: int = 100000
  pulser.set_period(TEST_PERIOD)
  print("Debug: Period")
  print("       Should be outputting period of " + str(TEST_PERIOD) + ".")
  print("       Is outputting period of        " + str(pulser.get_period_us()) + ".")
  sleep(10)
  del TEST_PERIOD

  # Stopping pulser
  print("DEBUG: Stopping pulser.")
  pulser.stop_pulser()
  print("DEBUG: Pulser stopped.")
