from time import sleep, time
import sys
import keyboard
import obd_comm as oc
import pulser.pulser_raspberry as pr
import pulser.pulser_nucleo as pn
import csv

FREQUENCY_MULTIPLIER: int = 10

OBD_PORT_NAME: str = '/dev/ttyUSB0'
NUCLEO_PORT_NAME: str = '/dev/ttyACM0'

R_SELECT: bool = '-raspberry' in sys.argv
N_SELECT: bool = '-nucleo'    in sys.argv

R_N_SELECT: bool = R_SELECT or N_SELECT

if R_SELECT and N_SELECT:
  print("ERROR: You can not output trough both Raspberry and Nucleo.")
  exit(-1)

obd: oc.OBDReader = oc.OBDReader(comport=OBD_PORT_NAME, baudrate=38400, timeout=5.0, end='\r')



if not obd.restart_setup():
  print("ERROR: Protocol has not been found.")
  exit(-1)
obd.start_updates()

f = open("ingore_speed_throttle_log.csv", 'a')
writer = csv.writer(f)
old_read_time: float = 0
new_read_time: float = 0 

sleep(5)

if R_SELECT:
  pulser: pr.PulserRaspberry = pr.PulserRaspberry(17)
elif N_SELECT:
  pulser: pn.PulserNucleo = pn.PulserNucleo(NUCLEO_PORT_NAME)

if R_N_SELECT:
  if not pulser.start_pulser():
    print("ERROR: Pulser has not started.")
    exit(-1)
  

while not keyboard.is_pressed('q'):
  
  speed: int = obd.get_speed()
  throttle:int = obd.get_throttle()
  new_read_time: float = time()

  if R_N_SELECT:
    pulser.set_frequency(FREQUENCY_MULTIPLIER*speed)

  writer.writerow([new_read_time, speed, throttle])
  print("Speed [kmph]:", speed, "Trottle [%]:", throttle, "   ", end='\r')
  old_read_time = new_read_time
  
  while time() - old_read_time < 0.1:
    pass

if R_N_SELECT:
  pulser.stop_pulser()

f.close()
