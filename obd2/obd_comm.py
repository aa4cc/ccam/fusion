from time import sleep
import serial as s
import multiprocessing as mp

class OBDReader:

  class BoolLock:
    def __init__(self, default_state: bool = False) -> None:
      self.__state: bool = default_state
      self.__lock: mp._LockType = mp.Lock()
    
    def get_state(self) -> bool:
      self.__lock.acquire()
      ret: bool = self.__state
      self.__lock.release()
      return ret
    
    def set_state(self, state: bool) -> None:
      self.__lock.acquire()
      self.__state = state
      self.__lock.release()
  
  def __init__(self, comport: str, baudrate: int = 38400, timeout: float = 1.0, end: str = '\r') -> None:
    # default values fork fine with provided OBD-II connector

    # Open serial line
    self.__serial:s.Serial = s.Serial(port=comport, baudrate=baudrate, timeout=timeout)

    if not self.__serial.isOpen():
      print("ERROR:OBDReader: Could not open port " + comport + ".")
      exit(-1)

    self.__endprocess: OBDReader.BoolLock = OBDReader.BoolLock(default_state=True)

    self.__commprocess: mp.Process = mp.Process(target=self.__comm_loop)
    
    self.__end: str = end

    self.__speed: int = 0
    self.__throttle: int = 0

    self.__pipe_rec_speed, self.__pipe_send_speed = mp.Pipe(duplex=False)
    self.__pipe_rec_throttle, self.__pipe_send_throttle = mp.Pipe(duplex=False)
  
  def close(self) -> None:
    self.__serial.close()


  def flush_serial(self) -> None:
    self.__serial.flushOutput()
    self.__serial.flushInput()
  
  def set_protocol(self, num: int) -> None:
    self.send_command("AT TP" + str(num))
  
  def restart_setup(self) -> bool:
    # returns True if protocol if succesfully found

    stopped: bool = self.stop_updates()
    print("OBDReader: Restarting communication and searching for protocol. This can take up to 30 seconds.")

    # Restart ELM327
    self.send_command("ATZ")
    sleep(3.0)

    self.flush_serial()

    print("OBDReader: Device restarted, searching for protocol.")

    # Iterate trough possible protocols
    serial_found: bool = False
    for i in range(6):
      print("OBDReader: Seraching in protocol: " + str(i) + ".")
      self.set_protocol(i)
      sleep(0.5)
      for _ in range(10):
        if 'OK' in self.__serial.read_until(self.__end.encode('utf-8')).decode('utf-8'):
          break
      else:
        continue
      
      self.send_command('0100')
      sleep(0.5)
      for _ in range(10):
        response: str = self.__serial.read_until(self.__end.encode('utf-8')).decode('utf-8')
        if 'NO DATA' in response or 'ERROR' in response or 'UNABLE TO CONNECT' in response:
          break

        elif 'BUS INIT' in response:
          serial_found = True
          break

      if serial_found:
        print("OBDReader: Protocol has been found.")
        break
      self.flush_serial()
    if stopped:
      self.start_updates()
    
    if not serial_found:
      print("OBDReader: Protocol has not been found.")

    return serial_found
  
  def start_updates(self) -> bool:
    if not self.__commprocess.is_alive():
      print("OBDReader: Updates started.")
      self.__endprocess.set_state(False)
      self.__commprocess.start()
      return True
    return False

  def stop_updates(self) -> bool:
    if self.__commprocess.is_alive():
      print("OBDReader: Updates stopped.")
      self.__endprocess.set_state(True)
      self.__commprocess.join()
      return True
    return False

  def send_command(self, command: str)-> None:
    stopped: bool = self.stop_updates()
    self.__serial.write( (command + self.__end).encode('utf-8'))
    if stopped:
      self.start_updates()

  def __comm_loop(self) -> None:
    def hexacheck(s: str) -> bool:
      allowed: list[str] = ['0', '1', '2', '3','4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E']
      return len(s) == 2 and s[0] in allowed and s[1] in allowed

    while not self.__endprocess.get_state():

      line: str

      SPEED_CODE: str = '010D'
      SPEED_RETURN: str = '410D'
      self.__serial.write( (SPEED_CODE + self.__end).encode('utf-8') )
      for _ in range(10):
        line = self.__serial.read_until(self.__end.encode('utf-8')).decode('utf-8').replace(" ", "").replace("\r", "")
        if SPEED_RETURN in line:
          spl: str = line.split(SPEED_RETURN)[-1][:2]
          if hexacheck(spl):
            self.__pipe_send_speed.send(int(spl, 16))
          break
      
      THROTTLE_CODE: str = '0111'
      THROTTLE_RETURN: str = '4111'
      self.__serial.write((THROTTLE_CODE + self.__end).encode('utf-8'))
      for _ in range(10):
        line = self.__serial.read_until(self.__end.encode('utf-8')).decode('utf-8').replace(" ", "").replace("\r", "")
        if THROTTLE_RETURN in line:
          spl: str = line.split(THROTTLE_RETURN)[-1][:2]
          if hexacheck(spl):
            self.__pipe_send_throttle.send(int(spl, 16))
          break
    
  def get_speed(self) -> int:
    speed: int = self.__speed
    while self.__pipe_rec_speed.poll(0):
      speed = self.__pipe_rec_speed.recv()
    self.__speed = speed
    return speed
  
  def get_throttle(self) -> int:
    throttle: int = self.__throttle
    while self.__pipe_rec_throttle.poll(0):
      throttle = self.__pipe_rec_throttle.recv()
    self.__throttle = throttle
    return throttle

if __name__ == "__main__":
  obd: OBDReader = OBDReader(comport="/dev/ttyUSB0", baudrate=38400, timeout=1.0/10.0, end='\r')

  if obd.restart_setup():
    print("OBD setup done.")
  else:
    print("OBD setup failed.")
    exit(-1)
  
  sleep(3)
  
  obd.start_updates()

  while True:
    print("Speed:", obd.get_speed(), "Throttle:", obd.get_throttle(), end='\r')
    sleep(0.2)
