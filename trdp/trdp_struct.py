import struct

def unpack(payload: bytes) -> tuple[float, int, int, int, int, int, int, int]:
  return struct.unpack("fIhHHHiL", payload)


#float speed_FDI;              ///< Speed calculated from digital inputs from odometry
#u_int32_t teethCountAbsolute; ///< Absolute odometry teeth counter
#int16_t speed_VCU;            ///< Reference speed obtained from VCU
#uint16_t teethCountRelative;  ///< Number of teeth counted from the last message
#uint16_t weight;              ///< Current weight of the tram
#uint16_t reserved;            ///< Alignment
#int32_t force;                ///< Current traction or braking force
#TimeDate64 timestamp;         ///< Timestamp of the message


if __name__ == "__main__":
  tags = 'fIhHHHiL'

  payload = struct.pack(tags, 1.0, 2, 3, 4, 5, 6, 7, 8)

  print(struct.calcsize(tags), ":", len(payload))

  print(unpack(payload))

class TRDP: # TODO
  def __init__(self) -> None:
    pass

  def get_speed(self) -> int:
    return 0
  
  def close(self) -> None:
    pass
