def coords_int2float(degrees: int|float = 0.0, minutes: int|float = 0.0, seconds: int|float = 0.0) -> float:
  return degrees + minutes/60 + seconds/3600
  