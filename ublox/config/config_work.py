from types import NoneType
import pyubx2
from ..communicators import UBXCommunicator, ParallelCommunicator
import json
import copy

POLL_LAYER_LIST: list[int] = [pyubx2.POLL_LAYER_RAM, pyubx2.POLL_LAYER_BBR , pyubx2.POLL_LAYER_FLASH, pyubx2.POLL_LAYER_DEFAULT]
SET_LAYER_LIST: list[int] = [pyubx2.SET_LAYER_RAM, pyubx2.SET_LAYER_BBR , pyubx2.POLL_LAYER_FLASH]

CFG_INDEX_RAM: int = 0
CFG_INDEX_BBR: int = 1
CFG_INDEX_FLASH: int = 2
CFG_INDEX_DEFAULT: int = 3

def pyubx_poll2cfg_index(pyubx_poll: int) -> int:
  if pyubx_poll == pyubx2.POLL_LAYER_RAM:
    return CFG_INDEX_RAM
  elif pyubx_poll == pyubx2.POLL_LAYER_BBR:
    return CFG_INDEX_BBR
  elif pyubx_poll == pyubx2.POLL_LAYER_FLASH:
    return CFG_INDEX_FLASH
  elif pyubx_poll == pyubx2.POLL_LAYER_DEFAULT:
    return CFG_INDEX_DEFAULT
  else:
    return -1

def pyubx_set2cfg_index(pyubx_poll: int) -> int:
  if pyubx_poll == pyubx2.SET_LAYER_RAM:
    return CFG_INDEX_RAM
  elif pyubx_poll == pyubx2.SET_LAYER_BBR:
    return CFG_INDEX_BBR
  elif pyubx_poll == pyubx2.SET_LAYER_FLASH:
    return CFG_INDEX_FLASH
  else:
    return -1

CFGMSG_DUPLICATE_LIST: list[str] = ["CFG_BDS_USE_PRN_1_TO_5"]


# HELPER FUNCTIONS

def iterate_cfgmsg_names() -> str:
  for cfgmsg_name in pyubx2.ubcdb.UBX_CONFIG_DATABASE:
    if cfgmsg_name in CFGMSG_DUPLICATE_LIST:
      continue
    yield cfgmsg_name

def bytes2bin(b: bytes) -> str:
  out: str = str()
  for x in b:
    out += "{:0>8}".format(bin(x)[2:])
  return out

def bin2bytes(b: str) -> bytes:
  return int(b, base=2).to_bytes(len(b)//8, byteorder='big', signed=False)
    
class UBXConfig:
  def __init__(self, dictionary: dict[str, list[int|float|str|NoneType]]) -> None:
    self.__dictionary = copy.deepcopy(dictionary)
  
  @staticmethod
  def get_from_ublox(communicator: UBXCommunicator|ParallelCommunicator, send_list_chunks: int = 32) -> object:

    def get_settings(communicator: UBXCommunicator|ParallelCommunicator, layer: int, send_list: list[str], save_dictionary: dict[str, int|str]) -> bool:
      if type(communicator) == ParallelCommunicator:
        communicator.write(pyubx2.UBXMessage.config_poll(layer, 0, send_list))
      else:
        communicator.write_message(pyubx2.UBXMessage.config_poll(layer, 0, send_list))

      got_val: bool = False
      got_ack: bool = False
      while not got_val or not got_ack:
        parsed_data = communicator.read_message()
        if parsed_data.identity == "ACK-ACK":
          got_ack = True
        elif parsed_data.identity == "ACK-NAK":
          return False
        elif parsed_data.identity == "CFG-VALGET" and "layer" in parsed_data.__dict__ and layer == parsed_data.__dict__["layer"]:
          for key in send_list:
            if key in parsed_data.__dict__:
              got_val = True
              if type(parsed_data.__dict__[key]) == bytes:
                save_dictionary[key][pyubx_poll2cfg_index(layer)] = bytes2bin(parsed_data.__dict__[key])
              elif type(parsed_data.__dict__[key]) == int or type(parsed_data.__dict__[key]) == str or type(parsed_data.__dict__[key]) == float:
                save_dictionary[key][pyubx_poll2cfg_index(layer)] = parsed_data.__dict__[key]
              else:
                print("ERROR: Unknown recieved type:", type(parsed_data.__dict__[key]))
            else:
              print("ERROR: Key missing while reading from u-blox.")
      return got_val and got_ack
    
    dictionary = dict()
    for cfgmsg_name in iterate_cfgmsg_names():
      dictionary[cfgmsg_name] = 4*[None]
    
    send_list: list[str] = list()
    for cfgmsg_name in dictionary:
      if len(send_list) >= send_list_chunks:
        for layer in POLL_LAYER_LIST:
          if not get_settings(communicator, layer, send_list, dictionary):
            for each_cfgmsg_name in send_list:
              get_settings(communicator, layer, [each_cfgmsg_name], dictionary)
        send_list = list()
      send_list.append(cfgmsg_name)
    return UBXConfig(dictionary)

  @staticmethod
  def get_from_json(file_name: str) -> object:
    dictionary: dict[str, list[int|str|None]] = dict()
    file = open(file_name, 'r')
    dictionary = json.loads(file.read())
    file.close()
    return UBXConfig(dictionary)

  def save_to_json(self, file_name: str) -> None:
    file = open(file_name, 'w')
    file.write(json.dumps(self.__dictionary).replace("],", "],\n"))
    file.close()

  def set_into_ublox(self, communicator: UBXCommunicator|ParallelCommunicator, send_list_chunks: int = 32) ->  None:
    for cfgmsg_name  in self.__dictionary:
      for layer in SET_LAYER_LIST:
        load: int|float|str|None|bytes = self.__dictionary[cfgmsg_name][pyubx_set2cfg_index(layer)]
        if type(load) == NoneType and load == None:
          continue
        elif type(load) == str:
          load = bin2bytes(load)
        if type(communicator) == ParallelCommunicator:
          communicator.write(pyubx2.UBXMessage.config_set(layer, 0, [(cfgmsg_name, load)]))
        else:
          communicator.write_message(pyubx2.UBXMessage.config_set(layer, 0, [(cfgmsg_name, load)]))
  