import ublox

FILE_NAME: str = "gitignore_test.json"


communicator = ublox.UBXCommunicator(ublox.find_port("u-blox"), ublox.FASTEST_BAUDRATE, 2.0)
conf: ublox.UBXConfig = ublox.UBXConfig.get_from_json(FILE_NAME)
conf.set_into_ublox(communicator)
