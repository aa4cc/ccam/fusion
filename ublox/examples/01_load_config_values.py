import ublox

FILE_NAME: str = "gitignore_test.json"

print("Warning: Can take up to 10 minutes to load.")
communicator = ublox.UBXCommunicator(ublox.find_port("u-blox"), ublox.FASTEST_BAUDRATE, 2.0)
conf: ublox.UBXConfig = ublox.UBXConfig.get_from_ublox(communicator)
conf.save_to_json(FILE_NAME)
