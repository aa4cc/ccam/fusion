from serial import Serial
from pyubx2 import UBXReader, UBXMessage

BASE_TIMEOUT: float = 2.0
FASTEST_BAUDRATE: int = 921600

class UBXCommunicator:
  def __init__(self, comport: str, baudrate: int, timeout: float) -> None:
    self.__serial: Serial = Serial(port=comport, baudrate=baudrate, timeout=timeout)
    self.__reader: UBXReader = UBXReader(self.__serial, protfilter=2)
  
  def read(self, size: int = 1) -> bytes:
    return self.__serial.read(size)

  def write(self, data: bytes) -> None:
    self.__serial.write(data)

  def read_message(self) -> UBXMessage:
    return self.__reader.read()[1]

  def write_message(self, message: UBXMessage) -> None:
    self.__serial.write(message.serialize())

  def close(self) -> None:
    self.__serial.close()
  
  def flush(self) -> None:
    self.__serial.flushInput()
    self.__serial.flushOutput()
