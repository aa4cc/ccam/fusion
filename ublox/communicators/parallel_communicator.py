from datetime import datetime
import multiprocessing as mp
import multiprocessing.connection
import threading as th
from time import sleep, time_ns
import pyubx2

from .ubx_communicator import UBXCommunicator

class ParallelCommunicator:
  """Communicator for writing and reading in separate processes."""

  class PriorityQueue:
    def __init__(self, priority_layers: int = 1) -> None:
      self.__layers: int = priority_layers
      self.__queue_list: list[mp.Queue] = list()
      for i in range(self.__layers):
        self.__queue_list.append(mp.Queue())
    
    def put(self, payload: object, priority: int = -1) -> bool:
      """Put object into priority queue, returns true on success, false otherwise."""
      if type(priority) != int or (priority < -1 or priority >= self.__layers):
        return False
      self.__queue_list[priority].put(payload)
      return True

    def get(self) -> object:
      """Get object from priority queue, by order."""
      for queue in self.__queue_list:
        if not queue.empty():
          return queue.get(block=True, timeout=None)
      return None

  class ProcessCommunicator:
    def __init__(self, name: str, value: object) -> None:
      self.name: str = name
      self.value: str = value

  def __init__(self, comport: str, baudrate: int, timeout: float, log: bool = True) -> None:
    self.__write_queue  = ParallelCommunicator.PriorityQueue(2)
    self.__read_queue   = ParallelCommunicator.PriorityQueue(1)

    process_reciever, self.__process_sender = mp.Pipe(duplex=False)
    self.__log: bool = log

    self.__process = mp.Process(target = self._process_function, args=(comport, baudrate, timeout, process_reciever, self.__log))

    self.__started = False
    self.__log = log

  def start(self) -> bool:
    """Starts the read/write process and returns True, if process already started, it returns False."""
    if self.__started:
      return False
    self.__process.start() 
    self.__started = True
    
    return True
  
  def stop(self) -> None:
    """Stops read/write prosess."""
    self.__process_sender.send(ParallelCommunicator.ProcessCommunicator('end', True))
    sleep(1.0)
    self.__process.kill()
    self.__process.join()
  
  def write(self, payload: pyubx2.UBXMessage|bytes, priority: int = -1) -> bool:
    """Sends data to the priority queue bound for sending."""
    return self.__write_queue.put(payload, priority)
  
  def read(self) -> pyubx2.UBXMessage|None:
    """Reads message or returns None if no message available."""
    return self.__read_queue.get()
    
  def _process_function(self, comport: str, baudrate: int, timeout: float, process_reciever: multiprocessing.connection.Connection, log: bool) -> None:

    def create_timestamped_log(payload: str, ubx_timestamp: int = -1) -> str:
      return str(datetime.now()) + ", " + str(time_ns() * (10**6)) + ", " + str(ubx_timestamp) + ", " + payload + "\n"

    class ThreadEnder:
      def __init__(self) -> None:
        self.__writing_end = False
        self.__writing_lock = th.Lock()

        self.__reading_end = False
        self.__reading_lock = th.Lock()
      
      def end(self) -> None:
        self.__writing_lock.acquire()
        self.__writing_end = True
        self.__writing_lock.release()

        self.__reading_lock.acquire()
        self.__reading_end = True
        self.__reading_lock.release()
      
      def end_writing(self) -> bool:
        self.__writing_lock.acquire()
        ret: bool = self.__writing_end
        self.__writing_lock.release()
        return ret
      
      def end_reading(self) -> bool:
        self.__reading_lock.acquire()
        ret: bool = self.__reading_end
        self.__reading_lock.release()
        return ret

    def write_function(communicator: UBXCommunicator, write_queue: ParallelCommunicator.PriorityQueue, ender: ThreadEnder, log) -> None:
      print("Parallel reciever: write function started.")
      if log:
        file = open("parallel_write.log",'a')
      while not ender.end_writing():
        payload: pyubx2.UBXMessage|bytes|None = write_queue.get()
        if type(payload) == pyubx2.UBXMessage:
          communicator.write_message(payload)
          if log:
            file.write( create_timestamped_log(str(payload)) )

        elif type(payload) == bytes:
          communicator.write(payload)
          if log:
            file.write( create_timestamped_log("RAW:0x" + str(bytes(payload).hex())) )
      
      if log:
        file.close()
      print("Parallel reciever: write function ended.")

    def read_function(communicator: UBXCommunicator, read_queue: ParallelCommunicator.PriorityQueue, ender: ThreadEnder, log) -> None:
      print("Parallel reciever: read function started.")
      if log:
        file = open("parallel_read.log",'a')

      while not ender.end_reading():
        message = communicator.read_message()
        
        if log:
          file.write( create_timestamped_log(str(message)) )
        read_queue.put(message)
      
      if log:
        file.close()
      print("Parallel reciever: read function ended.")

    communicator = UBXCommunicator(comport, baudrate, timeout)
    ender = ThreadEnder()

    writing_thread = th.Thread(target=write_function, args=(communicator, self.__write_queue, ender, log))
    reading_thread = th.Thread(target=read_function, args=(communicator, self.__read_queue, ender, log))

    writing_thread.start()
    reading_thread.start()

    while True:
      rec: ParallelCommunicator.ProcessCommunicator = process_reciever.recv()
      if type(rec) == ParallelCommunicator.ProcessCommunicator and rec.name == 'end' and rec.value == True:
        ender.end()
        break
    print("Parallel reciever: waiting for writing join.")
    writing_thread.join()
    print("Parallel reciever: writing joined.")

    print("Parallel reciever: waiting for reading join.")
    reading_thread.join()
    print("Parallel reciever: reading joined.")

    return None
