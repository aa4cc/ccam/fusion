import serial.tools.list_ports as lp
import re

def find_port(device_regex: str) -> str:
  """Returns first port, whose description matches regex string given."""
  for port_name_out in lp.comports():
    port_name: list[str] = str(port_name_out).strip().split(" - ")
    if re.search(device_regex, port_name[1]):
      return port_name[0]
  
  return ""
