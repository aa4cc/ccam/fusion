#****************************************************************************#
#this script can be used to parse the data stored in NAV-PVT messages to csv #
#****************************************************************************#

#YOU HAVE TO EDIT THIS PART
#input file
FILENAME = "experiment1_1/parsed_read.log"
#directory where output will be located
OUTPUT = "parsed/navmsgdata.txt"





def read_num(string):
    numbers = "0123456789."
    ret = ""
    x = 0
    while string[x] in numbers:
        ret += string[x]
        x += 1

    return ret



def parse_nav_msg(msg: str):
    if msg[:len("<UBX(NAV-PVT")] == "<UBX(NAV-PVT":
        ret = ""
        for x in range(43): #43 = number of datafields in NAV-PVT message
            msg = msg[msg.find("=")+1 : ]
            ret+=str(read_num(msg))+";"
        ret += read_num(msg[msg.find("> ") +2:])
        return ret


file = open(FILENAME, 'r')
fout = open(OUTPUT, 'w')
fout.write("iTOW, year, month, day, hour, min, second, validDate, validTime, fullyResolved, validMag, tAcc, nano, fixType, gnssFixOk, difSoln, psmState, headVehValid, carrSoln, confirmedAvai, confirmedDate, confirmedTime, numSV, lon, lat, height, hMSL, hAcc, vAcc, velN, velE, velD, gSpeed, headMot, sAcc, headAcc, pDOP, invalidLlh, lastCorrectionAge, reserved0, headVeh, magDec, magAcc, GNSStime".replace(" ", ""))
for line in file.readlines():
    ret = parse_nav_msg(line)
    if ret:
        fout.write(ret+"\n")


file.close()
fout.close()