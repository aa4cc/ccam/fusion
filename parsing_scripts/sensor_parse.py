"""
the purpose of this script is to parse the data from a file of ESF-MEAS messages (INPUT_FILE env variable)
where each line is one message 
and output it into files named str(OUTPUT_FILE+nameofsensor+".txt") 
it is not yet finished
"""

from gnss_com import sensor_data_to_signed as un2signed
from gnss_com_keys import sensor_types
from utils import read_num

from collections import namedtuple
from math import e as euler

#environment variables
INPUT_FILE = "experiment1_1/parsed_read.log"
OUTPUT_FILE = "sensordata"
WANTED_SENSORS = [5,11,12,13,14,15,16,17,18] #ids of sensors whose data I care about


Measurement = namedtuple('Measurement', ['sen_id', 'value'])




def parse_meas_msg(msg: str) -> (int, list):#-> timetag, [(sensor_id, sensor_value), (sensor_id, sensor_value), .... ]
    if msg[:len("<UBX(ESF-MEAS")] == "<UBX(ESF-MEAS":
        timetag_start=msg.find("> ")+len("> ")
        timetag_start = msg[timetag_start:].find(", ") +2 + timetag_start
        timetag = None
        if timetag_start > 10:
            timetag = float(read_num(msg[timetag_start:]))
            timetag = timetag*1000 - 1662854382419 + 1662847182000+7200000
            timetag = str(timetag)
        measurements = []
        unread_measurements = True
        x = 1
        while unread_measurements:
            cur_number = str(x)
            if x < 10:
                cur_number = "0"+cur_number
            val_start = msg.find("dataField_"+cur_number)
            if val_start > 0:
                value = read_num(msg[val_start+len("dataField_00="):])
                val_start = msg.find("dataType_"+cur_number)
                sen_id = read_num(msg[val_start+len("dataType_00="):])
                measurements.append(Measurement(sen_id, value))
                x += 1
            else:
                unread_measurements = False
        return timetag, measurements
    else:
        return None, None

#the value displayed in ESF-MEAS message parsed by pyubx2 lib is always interpreted as unsigned int
# (example of ESF-MEAS: <UBX(ESF-MEAS, timeTag=13095971, timeMarkSent=0, timeMarkEdge=0, calibTtagValid=1, numMeas=1, id=0, dataField_01=1264, dataType_01=14>) 
#but the values are sometimes signed, in addition to that, they are not in correct units, 
#this fnction aims to convert the value given by the ESF-MEAS message as parsed in pyubx2 and convert it to its actuall value
#NOT TESTED PROPERLY, 
#if you have suspicion that it returns wrong values, please check it together with the sensor_data_to_signed function from gnss_com
#or consider using raw data instead
#conv_to_si: if True, it converts the value to SI units (recommended False for more precise measurements)
def convert_value(sen_id: int, value: int, conv_to_si: bool) -> float:
    #print(sen_id)
    gyro_ids = [5,13,14]
    accel_ids = [16,17,18]
    gyro_temperature = 12 
    actual_value = None
    #TODO implement the conversions for the rest of sensors (should you ever need them) 
    if sen_id in gyro_ids:
        actual_value = un2signed(value)/(2**(12*conv_to_si))#deg/s*2^-12, -> deg/s
    elif sen_id in accel_ids:
        actual_value = un2signed(value)/(2**(10*conv_to_si))
    elif sen_id == gyro_temperature:
        actual_value = un2signed(value)/(euler**(2*conv_to_si))
    return actual_value



input_file = open(INPUT_FILE, 'r')
print("reading lines")
msgs = input_file.readlines()
print("lines read")
input_file.close()
output_files = {}
for x in WANTED_SENSORS:
    output_files[x] = open("parsed/"+OUTPUT_FILE+"_"+sensor_types[x].replace(" ", "_")+".txt", 'w')


output_files[11].write("testing")

minlen = len("<UBX(ESF-MEAS, timeTag")
a = 0
for msg in msgs:
    if len(msg) > minlen:
        (timetag, measurements) = parse_meas_msg(msg)

        if timetag:
            for x in measurements:
                if int(x.sen_id) in WANTED_SENSORS:
                    print(x.sen_id)
                    
                    output_files[int(x.sen_id)].write(str(timetag)+", "+str(convert_value(int(x.sen_id),int(x.value),True))+"\n")

for x in WANTED_SENSORS:
    output_files[x].close()